# Virtual Race

It is a small, self-contained project to organise virtual races, tours, challenges of many sports (like running, cycling, hiking, swimming or rowing). It was created to give some motivation to groups that cannot train together, or to track the total work.

The basic functionality allows submitting distances. After adding them to the totals and displaying the leaderboard of all participants, the longest activities are also tracked and displayed. Position of each competitor is displayed on the map with coloured markers.

Further features:

* Opional [registration](docs/FEATURES.md#registration)
* [Check points or milestones](docs/FEATURES.md#targets) can also be added. The first three to complete them get medals automatically that are displayed by their names.
* Activities over certain distances can be awarded [(challenges)](docs/FEATURES.md#challenges).
* [Monthly results](docs/FEATURES.md#monthly) can be awarded with medals for each month.
* The current participant with the most activities is honoured with a title.
* [Joint Effort](docs/FEATURES.md#joint). A target distance participants have to reach together.
* [Tug of War](docs/FEATURES.md#tow). Activities are aggregated for two competing groups (membership is configured for each user), like girls vs boys, sons vs fathers, etc.
* [Blackout](docs/FEATURES.md#targets). Participants see only their own results during a blackout.
* Finishers may download a [Participation Paper](docs/FEATURES.md#paper) (or Certificate of Merit, or Diploma) displaying their result details and showing the track with their steps.
* A simple [Admin UI](docs/ADMINUI.md) to ease overview/correction of data and maintenance.

You can try a **live demo [here](https://sapikli.gitlab.io/virtual-race/sample-spartathlon/)**. (Data is cleared regularly so that others may get medals.)

---

The `docs` folder contains a [step-by-step guide](docs/GUIDE.md) to set up a new race. There is also a [information on customising templates](docs/TEMPLATE.md) to your needs. A summary of features and how to configure them is explained [here](docs/FEATURES.md). A sample template is also included in `docs/sample-spartathlon`.  
The admin functions are documented [here](docs/ADMIN.md).

---

Panels of the site:

![Submit form](docs/vr_submit.png)

![Tug of War](docs/vr_tow.png)

![Latest](docs/vr_latest.png)

![Longest](docs/vr_longest.png)

![Result List](docs/vr_list.png)

![Map](docs/vr_map.png)

![Mine](docs/vr_mine.png)

![Admin UI](docs/adminui_users.png)

---

The project backend runs on a Google Cloud [App Engine](https://cloud.google.com/appengine/) using ECMAScript on [Node.js](https://nodejs.org/en/). Due to the low data amount persistent data is stored in a GC [Datastore](https://googleapis.dev/nodejs/datastore/latest/Datastore.html). No SQL server, MongoDB, Docker container, or virtual machine is required.

The frontend uses [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), [Bootstrap 5](https://getbootstrap.com/docs/5.1/getting-started/introduction/), [Leaflet](https://leafletjs.com/) with the [Leaflet.markercluster](https://github.com/Leaflet/Leaflet.markercluster) plugin.

Since this virtual race is meant for fun, or for a friendly game, no authentication is built in. Participants are identified by their (nick)names.

There are basic administrative functions available through an HTTP interface, like modifying or deleting an entry or user, or to list objects matching given filters.
