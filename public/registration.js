/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const baseURL = AppEngineURL + rootURL;
let	bkgColor = 'white';
let	userName = '';
let	userBgColor = '#ffff00';
let	userFgColor = '#ff0000';


function onInit()
{
	const	markerSVG = document.getElementById('markersymbols');
	if (markerSVG && MarkerSymbols)
		markerSVG.innerHTML = MarkerSymbols;

	const	okColors = [ '#000000', '#5F7D88', '#989898', '#CBCBCB', '#FFFFFF', '#663AB5', '#9A25B0', '#010088', '#6465B7', 
						'#0064B2', '#2095F2', '#24C0B8', '#274912', '#6D7E47', '#009683', '#4AAD4F', '#8AC04A', '#CDDA3A', 
						'#FEF301', '#FE9502', '#F47530', '#F5AA81', '#E77F74', '#D44021', '#FD0000', '#E61D61', '#EF96CE', 
						'#6F411A', '#8D3E29', '#A7262A', '#7E6747', '#B0A390' ];

	const	nameEdit = document.getElementById('nameEdit');
	bkgColor = nameEdit.style.backgroundColor;
	userName = nameEdit.value.trim();

	const	i = Math.floor(Math.random() * okColors.length);
	userBgColor = okColors[i];
	userFgColor = okColors[(i + Math.floor(okColors.length / 2)) % okColors.length];

	document.getElementById('bgcolor').value = userBgColor;
	document.getElementById('fgcolor').value = userFgColor;

	setMarkers(userName, userBgColor, userFgColor);
}


function onInput(ctrl)
{
	if (ctrl.style.backgroundColor !== bkgColor)
		ctrl.style.backgroundColor = bkgColor;

	document.getElementById('validateerror').style.display = 'none';
	document.getElementById('submiterror').style.display = 'none';
	document.getElementById('nameinuse').style.display = 'none';
	document.getElementById('submitsuccess').style.display = 'none';

	userName = ctrl.value
		.replace(new RegExp('{', 'g'), '[')
		.replace(new RegExp('}', 'g'), ']')
		.replace(new RegExp('_', 'g'), '-')
		.replace(new RegExp('/', 'g'), '-')
	ctrl.value = userName;
	userName = userName.trim();

	setMarkers(userName, userBgColor, userFgColor);
}


function onChangeBg(ctrl)
{
	userBgColor = ctrl.value;
	setMarkers(userName, userBgColor, userFgColor);
}


function onChangeFg(ctrl)
{
	userFgColor = ctrl.value;
	setMarkers(userName, userBgColor, userFgColor);
}


function showAlert(msg)
{
	document.getElementById('validatemsg').innerHTML = msg;
	document.getElementById('validateerror').style.display = 'block';
}


function save()
{
	userName = userName.trim();
	document.getElementById('nameEdit').value = userName;

	if (userName.length < 1) {
		showAlert(NonameAlert);
		document.getElementById('nameEdit').style.backgroundColor = '#FFDDCC';
		document.getElementById('nameEdit').focus();
		return false;
	}

	let		s = {
		rememberMe: document.getElementById('rememberCheck').checked,
		name: ''
	};
	if (s.rememberMe)
		s.name = userName;

	localStorage.setItem(lsName, JSON.stringify(s));

	const	details = {
		name: userName,
		bgcolor: userBgColor,
		fgcolor: userFgColor
	};

	let		official = document.getElementById('officialEdit').value.trim();
	if (official.length > 0)
		details.official = official;

	const	svg = getMarkerSVG();
	if (typeof svg === 'undefined')
		return false;
	else if (svg)
		details.svg = svg;

	let	team;
	radios = document.getElementsByName('team');
	if (radios) {
		for (i = 0; i < radios.length; ++i) {
			if (radios[i].checked) {
				team = parseInt(radios[i].value);
				break;
			}
		}
		if (team && !isNaN(team))
			details.team = team;
	}

	document.getElementById('submitButton').disabled = true;
	document.getElementById('submitting').style.display = 'block';
	document.getElementById('submiterror').style.display = 'none';
	document.getElementById('nameinuse').style.display = 'none';
	document.getElementById('submitsuccess').style.display = 'none';

	fetch(baseURL + '/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(details)
		})
		.then(res => res.json())
		.then(result => {
			document.getElementById('submitting').style.display = 'none';
			if (result.success) {
				document.getElementById('submiterror').style.display = 'none';
				document.getElementById('nameinuse').style.display = 'none';
				document.getElementById('submitsuccess').style.display = 'block';
			} else if (result.exists) {
				document.getElementById('submitButton').disabled = false;
				document.getElementById('submiterror').style.display = 'none';
				document.getElementById('nameinuse').style.display = 'block';
				document.getElementById('submitsuccess').style.display = 'none';
			} else {
				document.getElementById('submitButton').disabled = false;
				if (result.missing)
					console.error(`Server misses required field: ${result.missing}`);
				document.getElementById('submiterror').style.display = 'block';
				document.getElementById('nameinuse').style.display = 'none';
				document.getElementById('submitsuccess').style.display = 'none';
			}
		})
		.catch(err => {
			console.error(err);
			document.getElementById('submitButton').disabled = false;
			document.getElementById('submitting').style.display = 'none';
			document.getElementById('submiterror').style.display = 'block';
			document.getElementById('submitsuccess').style.display = 'none';
			distEdit.value = distance;
		});

	return false;
}


document.addEventListener('DOMContentLoaded', onInit);
