/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

function clickHandler()
{
	const	coord = this.getAttribute('data-coord');
	if (map && typeof coord === 'string' && coord.length >= 5) {
		try {
			let		coordval = JSON.parse(coord);
			if (Array.isArray(coordval) && coordval.length === 2)
				map.flyTo(coordval, 15);
		} catch(_err) {
		}
	}
}


class VRContent {
	constructor()
	{
		const	self = this;

		self.usOrdinal = (navigator.language && navigator.language.toLowerCase().startsWith('en'));
		self.numFormatter = new Intl.NumberFormat(undefined, { style: 'decimal' });
		self.dateFormatter = new Intl.DateTimeFormat();
	}

	static escapeHTML(s)
	{
		const	lookup = {
			'&': "&amp;",
			'"': "&quot;",
			'\'': "&apos;",
			'<': "&lt;",
			'>': "&gt;"
		};
		return s.replace( /[&"'<>]/g, c => lookup[c] );
	}

	getOrdinal(ordinal, bold)
	{
		const	self = this;

		let		before = '';
		let		after = '';
		if (bold) {
			before = '<b>';
			after = '</b>';
		}

		if (self.usOrdinal)
			return before + '#' + ordinal.toString() + after;
		else
			return before + ordinal.toString() + '.' + after;
	}

	static _userCell(item, bold, achievements)
	{
		let		before = '';
		let		after = '';
		if (bold) {
			before = '<b>';
			after = '</b>';
		}

		let	svg;
		if (item.svg) {
			svg = item.svg
				.replace(new RegExp('{{size}}', 'g'), '24')
				.replace(new RegExp('{{bgcolor}}', 'g'), `${item.bgcolor}`)
				.replace(new RegExp('{{fgcolor}}', 'g'), `${item.fgcolor}`);
		} else {
			svg = `<svg width="24" height="24" viewBox="0 0 32 32">` +
				`<use href="#mapmarker-bg" style="fill:${item.bgcolor};"/>` +
				`<use href="#mapmarker"/>` +
				`<use href="#mapmarker-fg" style="fill:${item.fgcolor};"/>` +
				`</svg>`;
		}

		let		cell = document.createElement('td');
		cell.className = 'left-text';

		cell.innerHTML = '<span class="iconpadding">' + svg + '</span>' + before + VRContent.escapeHTML(item.user) + after;
		if (achievements && achievements.length > 0)
			cell.innerHTML += ' ' + achievements;

		return cell;
	}

	_getDistanceText(dist)
	{
		const	self = this;

		const	text = self.numFormatter.format(dist / DistanceMultiplier);
		return VRContent.escapeHTML(text).replace(/\s/g, '&nbsp;');
	}

	formatDistanceSum(dist, round)
	{
		const	self = this;

		let		num = dist / (DistanceMultiplier * SumDivisor);
		if (round)
			num = Math.round(num);

		const	text = self.numFormatter.format(num);

		return VRContent.escapeHTML(text).replace(/\s/g, '&nbsp;');
	}

	_distanceSumCell(dist)
	{
		const	self = this;

		let		cell = document.createElement('td');
		cell.innerHTML = self.formatDistanceSum(dist);

		return cell;
	}

	getDateText(date)
	{
		const	self = this;

		const	text = self.dateFormatter.format(new Date(date));
		return VRContent.escapeHTML(text).replace(/\s/g, '&nbsp;');
	}

	static _getElem(id)
	{
		let	elem = document.getElementById(id);
		if (!elem)
			return null;

		while (elem.firstChild) {
			elem.removeChild(elem.firstChild);
		}

		return elem;
	}

	static _addIcon(icon, title)
	{
		let		text = '';

		if (title && title.length > 0) {
			let		s = VRContent.escapeHTML(title);
			if ([...s].length > 25) {
				let		i = Math.floor(s.length / 2);
				for ( ; i > 0; --i) {
					if (s[i] === ' ') {
						s = s.slice(0, i) + '\n' + s.slice(i + 1);
						break;
					} else if (s[s.length - i] === ' ') {
						s = s.slice(0, -i) + '\n' + s.slice(1 - i);
						break;
					}
				}
			}

			text = '<span title="' + s + '" tabindex="0">';
		}

		text += icon;

		if (title && title.length > 0)
			text += '</span>';

		return text;
	}

	static _formatBadges(badges)
	{
		if (!badges)
			return '';

		let		badgeStr = [];
		for (const badge of badges) {
			if (badge.finish)
				continue;

			let icon = badge.icon;
			if (badge.place >= 1 && badge.place <= 3)
				icon = config.targetmedals[badge.place - 1];
			if (!icon)
				continue;

			const	description = badge.description ?? badge.name;
			badgeStr.push(VRContent._addIcon(icon, description));
		}

		return badgeStr.join(' ');
	}

	static _formatChallenge(challengeResult)
	{
		if (!challengeResult)
			return '';

		return ' ' + VRContent._addIcon(challengeResult.icon, challengeResult.name);
	}

	setLatest(id, activities, user)
	{
		const	self = this;
		const	elem = VRContent._getElem(id);
		if (!elem || !activities || activities.length === 0)
			return;

		for (const act of activities) {
			let		row = elem.insertRow(-1);
			if (act.user === user)
				row.className = 'table-success';

			row.insertCell().innerHTML = VRContent.escapeHTML(act.user);
			row.insertCell().innerHTML = self._getDistanceText(act.distance) + VRContent._formatChallenge(act.challengeResult);
			row.insertCell().innerHTML = self.getDateText(act.date);
		}
	}

	setLongest(id, items, user)
	{
		const	self = this;
		const	elem = VRContent._getElem(id);
		if (!elem || !items || items.length === 0)
			return;

		let		i;
		for (i = 0; i < items.length; ++i) {
			let		row = elem.insertRow(-1);
			if (items[i].user === user)
				row.className = 'table-success';

			row.insertCell().innerHTML = self.getOrdinal(i + 1);
			row.insertCell().innerHTML = VRContent.escapeHTML(items[i].user);
			row.insertCell().innerHTML = self._getDistanceText(items[i].distance);
			row.insertCell().innerHTML = self.getDateText(items[i].date);
		}
	}

	formatMonthly(medals, icon, formatter)
	{
		const	self = this;

		if (!medals || medals.length === 0)
			return '';

		const	title = medals.map(item => {
			const	cell = self._distanceSumCell(item.distance);
			return formatter.format(new Date(item.month)) + ` [${cell.innerHTML}]`;
		}).join('; ');
		if (medals.length > 1)
			icon += `&#xD7;${medals.length}`;

		return ' ' + VRContent._addIcon(icon, title);
	}

	setList(id, items, tracklength, user)
	{
		const	self = this;
		const	elem = VRContent._getElem(id);
		if (!elem || !items || items.length === 0)
			return;

		let		maxcount = 0;
		for (const item of items) {
			if (maxcount < item.count)
				maxcount = item.count;
		}

		const	shortDateFormatter = new Intl.DateTimeFormat(undefined, { year: 'numeric', month: 'long' });
		let		i;
		const	firstDistance = items[0].distance;
		let		prevDistance = firstDistance;
		let		prevPlace = 0;
		for (i = 0; i < items.length; ++i) {
			const	item = items[i];

			let		row = elem.insertRow(-1);
			if (item.user === user)
				row.className = 'table-success';

			let		cell = row.insertCell();
			if (item.place) {
				if (item.place >= 1 && item.place <= 3)
					cell.innerHTML = config.trophies[item.place - 1];
				else
					cell.innerHTML = self.getOrdinal(item.place, false);
			} else {
				cell.innerHTML = self.getOrdinal(i + 1, (i < 3));
			}

			let		achievements = VRContent._formatBadges(item.badges);
			if (item.challengeResults && item.challengeResults.length > 0) {
				let		ch = {};
				for (const res of item.challengeResults) {
					if (ch[res.distance])
						++ch[res.distance].count;
					else 
						ch[res.distance] = { count: 1, distance: res.distance, icon: res.icon, name: res.name };
				}

				const	arr = Object.entries(ch);
				arr.sort((a, b) => b[0] - a[0]);

				for (const res of arr) {
					achievements += VRContent._formatChallenge(res[1]);
					if (res[1].count > 1)
						achievements += `&#xD7;${res[1].count}`;
				}
			}
			if (item.monthly) {
				achievements += self.formatMonthly(item.monthly.m1, config.monthlymedals[0], shortDateFormatter);
				achievements += self.formatMonthly(item.monthly.m2, config.monthlymedals[1], shortDateFormatter);
				achievements += self.formatMonthly(item.monthly.m3, config.monthlymedals[2], shortDateFormatter);
			}
			if (item.count >= 10 && item.count === maxcount && config.hardworksign && config.hardworksign.length > 0)
				achievements += ' ' + VRContent._addIcon(config.hardworksign, HardWorkText);

			let		userCell = VRContent._userCell(item, (i < 3), achievements);
			userCell.setAttribute('data-coord', JSON.stringify(item.pos));
			userCell.style.cursor = 'pointer';
			userCell.addEventListener('click', clickHandler);

			row.append(userCell);

			cell = self._distanceSumCell(item.distance);
			cell.innerHTML += ' <span class="small">(' + item.count.toString() + ')</span>';
			row.append(cell);

			if (item.place) {
				cell = row.insertCell();
				const	text = self.dateFormatter.format(new Date(item.finishdate));
				const	count = Math.floor(item.distance / tracklength);
				if (count >= 2)
					cell.innerHTML = VRContent._addIcon(`${config.completedsign}&#xD7;${count}`, text);
				else
					cell.innerHTML = VRContent._addIcon(config.completedsign, text);

				prevDistance = tracklength;
			} else if (i === 0 || item.distance === items[0].distance) {
				row.insertCell().innerHTML = config.leadersign;
				prevDistance = item.distance;
			} else {
				if (prevDistance !== item.distance) {
					cell = self._distanceSumCell(prevDistance - item.distance);
					if (prevDistance !== firstDistance) {
						const	text = self.numFormatter.format((firstDistance - item.distance) / (DistanceMultiplier * SumDivisor));
						cell.innerHTML += ' <span class="small">(' + VRContent.escapeHTML(text).replace(/\s/g, '&nbsp;') + ')</span>';
					}
					row.append(cell);
				} else {
					row.insertCell().innerHTML = '=';
				}
				prevDistance = item.distance;
				if (prevPlace)
					row.style.borderTop = '2px solid #808080';
			}

			prevPlace = item.place;
		}
	}

	setMine(id, activities)
	{
		const	self = this;
		const	elem = VRContent._getElem(id);
		let		mine = document.getElementById('mine');
		if (elem && mine) {
			if (activities && activities.length > 0)
				mine.style.visibility = 'visible';
			else
				mine.style.visibility = 'hidden';
		}
		if (!elem || !activities || activities.length === 0)
			return;

		for (const act of activities) {
			let		row = elem.insertRow(-1);
			row.insertCell().innerHTML = self.getDateText(act.date);
			row.insertCell().innerHTML = self._getDistanceText(act.distance) + VRContent._formatChallenge(act.challengeResult);

			let		cell = self._distanceSumCell(act.total);
			if (act.milestone) {
				let		defIcon = config.milestoneicon ?? '&#x1F6A9;';
				let		badge = Object.assign({}, act.milestone);
				if (act.milestone.finish) {
					defIcon = config.finishermedal;
					badge.description = Finish;
				}
				badge.icon = act.milestone.icon;
				if (!badge.icon || badge.icon.length === 0)
					badge.icon = defIcon;
				cell.innerHTML += ' &#x2794; ' + VRContent._formatBadges([badge]);
			}
			row.append(cell);
		}
	}
}
