/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const	RadioSVG = '<svg width="64" height="64">';

const	MarkerBG = Object.freeze({
	SVG: 			0,
	RoundedRect:	1,
	Circle:			2
});

// Return the amount required to offset the text vertically for middle alignment.
function getTextOffset(text, font)
{
	try {
		const	c = document.getElementById('myCanvas');
		let		ctx = c.getContext('2d');
		ctx.font = font;

		const	tmA = ctx.measureText('A');
		const	tm = ctx.measureText(text);
		const	offsA = (tmA.actualBoundingBoxAscent - tmA.actualBoundingBoxDescent) / 2;
		const	offs = (tm.actualBoundingBoxAscent - tm.actualBoundingBoxDescent) / 2;

		// It looks nicer if we shift by only half of the necessary extra ascent/descent
		// (like accents on capitals, cedillas, or the tail of Q).
		return (offs + offsA) / 2;
	}
	catch(err)
	{
		return 8;
	}
}


function createMarkerSVG(text, bg, userBgColor, userFgColor)
{
	// text: #id for a marker, or 1-letter or 2-letter initials

	let		svg = RadioSVG + '<svg viewBox="0 0 32 32">';

	switch (bg) {
		case MarkerBG.SVG:
			svg += `<use href="${text}-bg" style="fill:${userBgColor};"/>`;
			break;
		case MarkerBG.RoundedRect:
			svg += `<rect x="0" y="0" width="32" height="32" rx="8" ry="8" style="stroke:black;stroke-width:0.1px;fill:${userBgColor};"/>`;
			break;
		default:
			svg += `<circle cx="16" cy="16" r="16" style="stroke:black;stroke-width:0.1px;fill:${userBgColor};"/>`;
			break;
	}

	let		offs = 16;
	switch (text.length) {
		case 1:
			offs += getTextOffset(text, '24px Arial Bold');
			svg += `<text x="16" y="${offs.toFixed(1)}" class="initials1" style="fill:${userFgColor};">${text}</text>`;
			break;
		case 2:
			offs += getTextOffset(text, '18px Arial');
			svg += `<text x="16" y="${offs.toFixed(1)}" class="initials2" style="fill:${userFgColor};">${text}</text>`;
			break;
		default:
			svg += `<use href="${text}"/>`;
			svg += `<use href="${text}-fg" style="fill:${userFgColor};"/>`;
			break;
	}

	svg += '</svg>';

	return svg + '</svg>';
}


function setMarkers(name, userBgColor, userFgColor)
{
	let	e1 = document.getElementById('marker1');
	if (e1)
		e1.innerHTML = createMarkerSVG('#mapmarker', MarkerBG.SVG, userBgColor, userFgColor);
	else
		console.error('marker1 not found.');
	let	e2 = document.getElementById('marker2');
	if (e2)
		e2.innerHTML = createMarkerSVG('#mapmarker', MarkerBG.Circle, userBgColor, userFgColor);

	// Optional additional icons
	let		i;
	for (i = 3; ; ++i) {
		let	e = document.getElementById(`marker${i}`);
		if (!e)
			break;

		e.innerHTML = createMarkerSVG(`#mapmarker${i}`, MarkerBG.RoundedRect, userBgColor, userFgColor);
	}

	let 	setBack = false;
	let		eI2a, eI2b;
	let		hasI2a = false;
	let		hasI2b = false;
	if (name.length >= 1) {
		let	eI1r = document.getElementById('markerI1r');
		if (eI1r)
			eI1r.innerHTML = createMarkerSVG(name[0].toUpperCase(), MarkerBG.RoundedRect, userBgColor, userFgColor);
		let	eI1c = document.getElementById('markerI1c');
		if (eI1c)
			eI1c.innerHTML = createMarkerSVG(name[0].toUpperCase(), MarkerBG.Circle, userBgColor, userFgColor);

		if (name.length >= 2) {
			const	names = name.split(' ');

			eI2a = document.getElementById('markerI2a');
			if (names[0].length > 1 && eI2a) {
				eI2a.innerHTML = createMarkerSVG(name[0] + name[1], 1, userBgColor, userFgColor);
				hasI2a = true;
			}

			eI2b = document.getElementById('markerI2b');
			if (names.length > 1 && names[0][1] !== names[1][0] && eI2b) {
				eI2b.innerHTML = createMarkerSVG(names[0][0] + names[1][0], 1, userBgColor, userFgColor);
				hasI2b = true;
			}
		}
	} else {
		setBack = true;
	}
	if (!hasI2a && eI2a && eI2a.checked)
		setBack = true;
	if (!hasI2b && eI2b && eI2b.checked)
		setBack = true;

	if (setBack) {
		if (e1)
			e1.checked = true;
		else if (e2)
			e2.checked = true;
	}

	const	showI2 = (name.length >= 1 ? '' : 'none');
	let	rI1r = document.getElementById('mrI1r');
	if (rI1r)
		rI1r.style.display = showI2;
	let	rI1c = document.getElementById('mrI1c');
	if (rI1c)
		rI1c.style.display = showI2;

	let	rI2a = document.getElementById('mrI2a');
	if (rI2a)
		rI2a.style.display = (hasI2a ? '' : 'none');
	let	rI2b = document.getElementById('mrI2b');
	if (rI2b)
		rI2b.style.display = (hasI2b ? '' : 'none');
}


function selectMarker(svg)
{
	if (!svg) {
		document.getElementById('marker1').checked = true;
		return;
	}

	// Workaround for rounding errors between different browsers
	let		pos = svg.indexOf('<text x="16" y="');
	if (pos >= 0) {
		const	pos2 = svg.indexOf('"', pos + 16);
		svg = svg.slice(0, pos) + svg.slice(pos2);
	}

	let		radios = document.getElementsByName('marker');
	let		i;
	for (i = 0; i < radios.length; ++i) {
		const	label = document.getElementById(radios[i].value);
		if (label) {
			let		markerSVG = label.innerHTML;
			if (markerSVG.startsWith(RadioSVG)) {
				markerSVG = '<svg width="{{size}}" height="{{size}}">' + markerSVG.slice(RadioSVG.length);
				// Workaround for rounding errors between different browsers
				pos = markerSVG.indexOf('<text x="16" y="');
				if (pos >= 0) {
					const	pos2 = markerSVG.indexOf('"', pos + 16);
					markerSVG = markerSVG.slice(0, pos) + markerSVG.slice(pos2);
				}
				if (svg === markerSVG) {
					radios[i].checked = true;
					return;
				}
			}
		} else {
			console.error(`Label not found for radio[${i}] (${radios[i].value})`);
		}
	}

	document.getElementById('marker1').checked = true;
}


function getMarkerSVG()
{
	let		svg = null;
	let		radios = document.getElementsByName('marker');
	let		i;
	for (i = 0; i < radios.length; ++i) {
		if (radios[i].checked) {
			const	label = document.getElementById(radios[i].value);
			if (label) {
				if (radios[i].value !== 'marker1')		// It is the default, no need to store.
					svg = label.innerHTML;
			} else {
				console.error(`Label not found for radio[${i}] (${radios[i].value})`);
			}
			break;
		}
	}
	if (svg) {
		if (svg.startsWith(RadioSVG)) {
			svg = '<svg width="{{size}}" height="{{size}}">' + svg.slice(RadioSVG.length);
		} else {
			console.error(`Malformed SVG label for radio[${i}]`);
			return undefined;
		}
	}

	return svg;
}
