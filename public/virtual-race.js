/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const baseURL = AppEngineURL + rootURL;
let	config = {};
let	map;
let	startMarker;
let	finishMarker;
let	targetMarkers;
let	mineMarkers;
let	userMarkers;
let	bkgColor = 'white';
let	isSure = false;


function showAchievement(content, act, mine)
{
	const	firstDate = mine.at(-1).date;

	if (act.place && act.finishdate) {
		document.getElementById('raceplace').innerHTML = content.getOrdinal(act.place, true);
		if (act.place) {
			let	decoration = config.finishermedal;
			if (act.place >= 1 && act.place <= 3)
				decoration = config.trophies[act.place - 1];
			document.getElementById('racedecoration').innerHTML = ' ' + decoration;
		}

		let		sd = config.startdate;
		if (firstDate)
			sd = firstDate;
		const	d1 = new Date(sd).getTime() / 1000;
		const	d2 = new Date(act.finishdate).getTime() / 1000;
		document.getElementById('raceduration').innerHTML = `${Math.floor(1 + (d2 + 43200 - d1) / 86400)}`;

		document.getElementById('racecompleted').style.display = 'block';
	}

	if (!act.badges || act.badges.length === 0)
		return;

	const	newBadges = act.badges.filter(newItem => (newItem.distance > mine[0].total - mine[0].distance && newItem.distance <= mine[0].total));
	if (newBadges.length === 0 || newBadges[0].finish || newBadges[0].hidealert)
		return;

	const	description = newBadges[0].description ?? newBadges[0].name;
	let		dist = '?';
	if (newBadges[0].distance)
		dist = content.formatDistanceSum(newBadges[0].distance);

	if (newBadges[0].place >= 1 && newBadges[0].place <= 3) {
		document.getElementById('targetmedal').innerHTML = config.targetmedals[newBadges[0].place - 1];
		document.getElementById('medaldistance').innerHTML = dist;
		document.getElementById('medalname').innerHTML = VRContent.escapeHTML(description);
		document.getElementById('targetmedalist').style.display = 'block';
	} else {
		let	icon = '';
		if (newBadges[0].icon)
			icon = ' ' + newBadges[0].icon;
		document.getElementById('targetbadge').innerHTML = icon;
		document.getElementById('targetdistance').innerHTML = dist;
		document.getElementById('targetname').innerHTML = VRContent.escapeHTML(description);
		document.getElementById('targetcompleted').style.display = 'block';
	}
}


function setJoint(content, result)
{
console.log(`result.jointdone = ${result.jointdone}`);

	let		total = 0, segmentfrom, segmentto = 0, cnt = 0;
	let		suffix = '';
	let		i;
	if (Array.isArray (config.jointdistance)) {
		cnt = config.jointdistance.length;
		for (i = 0; i < cnt; ++i) {
			total += config.jointdistance[i] * DistanceMultiplier * SumDivisor;
			segmentfrom = segmentto;
			segmentto = total;
			suffix = '-' + (i + 1).toString();
			if (total >= result.jointdone)
				break;
		}
		for ( ; i < cnt; ++i) {
			total += config.jointdistance[i] * DistanceMultiplier * SumDivisor;
		}
	} else {
		total = config.jointdistance * DistanceMultiplier * SumDivisor;
		segmentfrom = 0;
		segmentto = total;
	}

	const	sd = new Date(config.startdate).getTime();
	if (result.jointdone >= total) {
		const	elem = document.getElementById('jointprogress');
		if (elem) {
			elem.style.display = 'none';
		} else {
			for (i = 0; i < cnt; ++i) {
				const	elem = document.getElementById(`jointprogress-${i + 1}`);
				if (elem)
					elem.style.display = 'none';
			}
		}

		let	days = 0;
		let	done = 0;
		for (const [k, v] of Object.entries(result.jointdays).sort()) {
			done += v;
			if (done >= total) {
				const	cd = new Date(k).getTime();
				days = 1 + Math.floor((cd - sd) / 86400000);
				break;
			}
		}
		document.getElementById('jointduration').innerHTML = days.toString();
		document.getElementById('jointcompleted').style.display = 'block';
	} else {
		document.getElementById('jointcompleted').style.display = 'none';

		const	doneStr = content.formatDistanceSum(result.jointdone - segmentfrom, true);
		const	remainingStr = content.formatDistanceSum(segmentto - result.jointdone, true);
		document.getElementById('jointdone' + suffix).innerHTML = doneStr;
		document.getElementById('jointremaining' + suffix).innerHTML = remainingStr;

		const	percent = 100 * (result.jointdone - segmentfrom) / (segmentto - segmentfrom);
		const	jointbar = document.getElementById('jointprogressbar' + suffix);
		if (jointbar) {
			jointbar.style.width = `${percent.toFixed(0)}%`;
			jointbar.setAttribute('aria-valuenow', percent.toFixed(0));
		}
		const	jointtext = document.getElementById('jointprogressbartext' + suffix);
		if (jointtext)
			jointtext.innerHTML = Math.floor(percent) + '%';

		const	jointeta = document.getElementById('jointeta' + suffix);
		if (result.jointdone > 0 && jointeta) {
			const	cd = Date.now();
			const	dur = (cd - sd) * segmentto / result.jointdone;
			const	eta = new Date(cd + dur);
			if (!isNaN(eta.getTime()))
				jointeta.innerHTML = content.getDateText(sd + dur);
		}
	
		const	elem = document.getElementById('jointprogress');
		if (elem) {
			elem.style.display = 'block';
		} else {
			for (i = 0; i < cnt; ++i) {
				const	s = '-' + (i + 1).toString();
				const	elem = document.getElementById('jointprogress' + s);
				if (elem)
					elem.style.display = (s === suffix ? 'block' : 'none');
			}
		}
	}
}


function setContent(content, result, user, afteradd)
{
	let tow = document.getElementById('tow');
	if (tow && result.tow) {
		let towbar1 = document.getElementById('towbar1');
		let towbar2 = document.getElementById('towbar2');
		const	percent = 100.0 * result.tow[0] / (result.tow[0] + result.tow[1]);
		towbar1.setAttribute('width', `${percent.toFixed(1)}%`);
		towbar2.setAttribute('x', `${percent.toFixed(1)}%`);
		towbar2.setAttribute('width', `${(100 - percent).toFixed(1)}%`);
		document.getElementById('towtext1').innerHTML = content.formatDistanceSum(result.tow[0]);
		document.getElementById('towtext2').innerHTML = content.formatDistanceSum(result.tow[1]);
		tow.style.display = 'block';
	}

	if (config.jointdistance)
		setJoint(content, result);

	content.setLatest('tbody_latest', result.latest, user);
	content.setLongest('tbody_longest', result.longest, user);
	content.setList('tbody_list', result.list, result.tracklength, user);
	content.setMine('tbody_mine', result.mine);

	document.getElementById('racecompleted').style.display = 'none';
	document.getElementById('targetcompleted').style.display = 'none';

	if (afteradd && result.myresult && result.mine && result.mine.length >= 1)
		showAchievement(content, result.myresult, result.mine);

	if (result.footer)
		document.getElementById('footer').innerHTML = result.footer;
}


function initMap(result)
{
	if (!document.getElementById('map'))
		return;

	if (!map) {
		map = L.map('map', { doubleClickZoom: true });
		if (ShowMetricBar || ShowImperialBar)
			L.control.scale({ metric: ShowMetricBar, imperial: ShowImperialBar }).addTo(map);

		L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: MapAttribution,
			maxZoom: 19
		}).addTo(map);

		fetch('track.json')
			.then(res => res.json())
			.then(response => {
				L.geoJson(response, { style: function (feature) {
					return {
						color: feature.properties.color ?? 'blue',
						dashArray: feature.properties.dashArray ?? '4',
						weight: feature.properties.weight ?? 1,
						fill: false
					};
				}})
				.addTo(map);
			})
			.catch(err => {
				console.error('geoJson error: ', err);
			});
	}

	if (result.trackbounds)
		map.fitBounds(result.trackbounds);

	if (!startMarker && result.startcoords) {
		startMarker = L.marker(result.startcoords).addTo(map);
		if (result.finishcoords)
			startMarker.bindPopup(`<b>${Start}</b>`);
		else
			startMarker.bindPopup(`<b>${StartFinish}</b>`);
	}
	if (!finishMarker && result.finishcoords && (typeof config.hidefinish === 'undefined' || !config.hidefinish)) {
		finishMarker = L.marker(result.finishcoords).addTo(map);
		finishMarker.bindPopup(`<b>${Finish}</b>`);
	}

	if (!targetMarkers && result.maptargets) {
		let		minTotal = 0;
		let		maxTotal = result.tracklength;
		let		maxtarget = 0;
		for (const target of result.maptargets) {
			if (maxtarget < target.distance)
				maxtarget = target.distance;
		}
		if (config.overflow && maxtarget > result.tracklength && result.mine && result.mine.length > 0) {
			const	total = result.mine[0].total;
			minTotal = result.tracklength * Math.floor(total / result.tracklength);
			maxTotal = result.tracklength * Math.ceil(total / result.tracklength);
		}

		targetMarkers = [];
		for (const target of result.maptargets) {
			if (target.finish || target.distance < minTotal || target.distance > maxTotal || !target.pos)
				continue;

			let	targetMarker;
			if (target.icon) {
				let	iconSize = 24;
				if (target.iconsize)
					iconSize = target.iconsize;
				targetMarker = L.divIcon({
					iconSize:		[iconSize, iconSize],
					html:			target.icon
				});
			} else {
				targetMarker = L.divIcon({
					iconSize:		[10, 10],
					className:		'target-marker'
				});
			}
			let	marker = L.marker(
				target.pos, { icon: targetMarker, title: target.name }
			);
			marker.bindPopup('<b>' + target.name + '</b>');
			marker.addTo(map);
			targetMarkers.push(marker);
		}
	}
}


function addMineMarker(content, item, fgcolor, bgcolor)
{
	let	marker = L.circleMarker(
		item.pos, { radius: 4, weight: 2, color: fgcolor ?? 'black', fill: true, fillColor: bgcolor ?? 'yellow', fillOpacity: 1.0 }
	);
	mineMarkers.addLayer(marker);

	marker.bindPopup(content.getDateText(item.date));
}


function addUserMarker(item, user, popup)
{
	let	size = MarkerSize;
	if (item.user === user)
		size = OwnMarkerSize;

	let	svg;
	if (item.svg) {
		svg = item.svg
			.replace(new RegExp('{{size}}', 'g'), `${size}`)
			.replace(new RegExp('{{bgcolor}}', 'g'), `${item.bgcolor}`)
			.replace(new RegExp('{{fgcolor}}', 'g'), `${item.fgcolor}`);
	} else {
		svg = `<svg width="${size}" height="${size}">` +
			`<use href="#mapmarker-bg" style="fill:${item.bgcolor};"/>` +
			`<use href="#mapmarker"/>` +
			`<use href="#mapmarker-fg" style="fill:${item.fgcolor};"/>` +
			`</svg>`;
	}

	let	markerIcon = L.divIcon({
		iconSize:		[size, size],
		iconAnchor:		[Math.floor(size * MarkerAnchorX / MarkerSize), Math.floor(size * MarkerAnchorY / MarkerSize)],
		popupAnchor:	[0, 0],
		html:			svg
	});

	let	marker = L.marker(
		item.pos, { icon: markerIcon, title: item.user, riseOnHover: true, zIndexOffset: (item.user === user ? 1000 : 100) }
	);
	userMarkers.addLayer(marker);
	if (popup)
		marker.bindPopup('<b>' + item.user + '</b><br>' + popup);
}


function setMarkers(content, result, user)
{
	if (!map)
		return;

	if (mineMarkers)
		map.removeLayer(mineMarkers);
	if (userMarkers)
		map.removeLayer(userMarkers);

	let	fgcolor, bgcolor;
	if (result.list && result.list.length > 0) {
		for (const item of result.list) {
			if (item.user === user) {
				fgcolor = item.fgcolor;
				bgcolor = item.bgcolor;
				break;
			}
		}
	}

	if (result.mine && result.mine.length > 0) {
		mineMarkers = L.layerGroup();
		for (const item of result.mine) {
			if (item.pos)
				addMineMarker(content, item, fgcolor, bgcolor);
		}
		map.addLayer(mineMarkers);
	}
	
	if (result.list && result.list.length > 0) {
		userMarkers = L.markerClusterGroup({ maxClusterRadius: Math.floor(MarkerSize / 2) });
		let	ordinal = 1;
		let	minLat = 1000.0;
		let	maxLat = -1000.0;
		let	minLon = 1000.0;
		let	maxLon = -1000.0;
		for (const item of result.list) {
			addUserMarker(item, user, `${ordinal}.<br>${content.formatDistanceSum(item.distance)}`);
			++ordinal;

			if (minLat > item.pos[0])
				minLat = item.pos[0];
			if (maxLat < item.pos[0])
				maxLat = item.pos[0];
			if (minLon > item.pos[1])
				minLon = item.pos[1];
			if (maxLon < item.pos[1])
				maxLon = item.pos[1];
		}
		map.addLayer(userMarkers);
		map.flyToBounds([ [ minLat, minLon ], [ maxLat, maxLon ] ]);
	}
}


function setSpinner(visible)
{
	let spinner = document.getElementById('spinner');
	spinner.style.display = (visible ? 'block' : 'none');
}


function loadContent(user, afteradd)
{
	setSpinner(true);

	let	params = {};
	if (user)
		params.user = user;

	fetch(baseURL + '/get', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(params)
		})
		.then(res => res.json())
		.then(result => {
			config = result.config ?? {};
			const	content = new VRContent();

			setContent(content, result, user, afteradd);
			if (result.blackout) {
				document.getElementById('latest').style.display = 'none';
				document.getElementById('list').style.display = 'none';
				document.getElementById('peloton').style.display = 'none';

				const	dt = new Date(new Date(config.enddate).getTime() + 86400000);	// config.enddate has the day only (e.g., 0:00:00) so we round it up to the next midnight.
				if (Date.now() < dt.getTime()) {
					if (document.getElementById('blackout'))
						document.getElementById('blackout').style.display = 'block';
					try {
						if (typeof startCountdown !== 'undefined')
							startCountdown(dt);
					}
					catch(_err) {
					}
				} else {
					if (document.getElementById('blackoutfinish'))
						document.getElementById('blackoutfinish').style.display = 'block';
				}
			} else {
				initMap(result);
				setMarkers(content, result, user);
			}

			setSpinner(false);
		})
		.catch(err => {
			console.error(err);
			setSpinner(false);
		});
}


function onInit()
{
	const	markerSVG = document.getElementById('markersymbols');
	if (markerSVG && MarkerSymbols)
		markerSVG.innerHTML = MarkerSymbols;

	const	s = localStorage.getItem(lsName);

	if (s && s.length > 0) {
		try {
			const	settings = JSON.parse(s);

			document.getElementById('rememberCheck').checked = settings.rememberMe;
			if (settings.rememberMe)
				document.getElementById('nameEdit').value = settings.name;
		} catch(err) {
		}
	}

	const	d = new Date().toISOString();
	document.getElementById('datePicker').value = d.slice(0, d.indexOf('T'));

	const	nameEdit = document.getElementById('nameEdit');
	bkgColor = nameEdit.style.backgroundColor;

	loadContent(nameEdit.value, false);
}


function onInput(ctrl)
{
	if (ctrl.style.backgroundColor !== bkgColor)
		ctrl.style.backgroundColor = bkgColor;

	document.getElementById('validateerror').style.display = 'none';
	document.getElementById('doublewarning').style.display = 'none';
	document.getElementById('accessdenied').style.display = 'none';
	document.getElementById('submiterror').style.display = 'none';
	document.getElementById('submitsuccess').style.display = 'none';
	isSure = false;
}


function showAlert(msg)
{
	document.getElementById('validatemsg').innerHTML = msg;
	document.getElementById('validateerror').style.display = 'block';
}


function save()
{
	const	name = document.getElementById('nameEdit').value
		.trim()
		.replace(new RegExp('{', 'g'), '[')
		.replace(new RegExp('}', 'g'), ']')
		.replace(new RegExp('_', 'g'), '-')
		.replace(new RegExp('/', 'g'), '-');
	document.getElementById('nameEdit').value = name;

	const	distEdit = document.getElementById('distanceEdit');
	const	distance = distEdit.value.trim();
	const	date = document.getElementById('datePicker').value.trim();

	if (!name || name.length < 1) {
		showAlert(NonameAlert);
		document.getElementById('nameEdit').style.backgroundColor = '#FFDDCC';
		document.getElementById('nameEdit').focus();
		return false;
	}
	if (!distance || distance.length < 1) {
		distEdit.style.backgroundColor = '#FFDDCC';
		distEdit.focus();
		return false;
	}
	const	distNum = parseFloat(distance);
	if (isNaN(distNum) || distNum < 0.1 || distNum > MaxDistance) {
		distEdit.style.backgroundColor = '#FFDDCC';
		distEdit.focus();
		return false;
	}
	if (DistanceMultiplier < 10 && distNum < 10) {
		distEdit.value = Math.floor(distNum * 1000.0).toString();
		distEdit.style.backgroundColor = '#FFDD44';
		distEdit.focus();
		return false;
	}

	const	d = new Date().toISOString();
	const	today = d.slice(0, d.indexOf('T'));
	if (date > today) {
		if (FutureAlert)
			showAlert(FutureAlert);
		document.getElementById('datePicker').style.backgroundColor = '#FFDDCC';
		document.getElementById('datePicker').focus();
		return false;
	}
	if (date < config.startdate) {
		showAlert(TooEarlyAlert);
		document.getElementById('datePicker').style.backgroundColor = '#FFDDCC';
		document.getElementById('datePicker').focus();
		return false;
	}
	if (date > config.enddate) {
		showAlert(TooLateAlert);
		document.getElementById('datePicker').style.backgroundColor = '#FFDDCC';
		document.getElementById('datePicker').focus();
		return false;
	}

	let		s = {
		rememberMe: document.getElementById('rememberCheck').checked,
		name: ''
	};
	if (s.rememberMe)
		s.name = name;

	localStorage.setItem(lsName, JSON.stringify(s));

	distEdit.value = '';
	document.getElementById('validateerror').style.display = 'none';
	document.getElementById('submitting').style.display = 'block';
	document.getElementById('accessdenied').style.display = 'none';
	document.getElementById('doublewarning').style.display = 'none';
	document.getElementById('submiterror').style.display = 'none';
	document.getElementById('submitsuccess').style.display = 'none';
	document.getElementById('challengemedal').style.display = 'none';

	let		distValue = Math.floor(distNum * DistanceMultiplier);
	fetch(baseURL + '/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ name, distance: distValue, date, repeated: isSure })
		})
		.then(res => res.json())
		.then(result => {
			document.getElementById('submitting').style.display = 'none';
			document.getElementById('submitsuccess').style.display = (result.success ? 'block' : 'none');
			if (result.success) {
				const	e = document.getElementById('submitdata');
				if (e)
					e.innerHTML = `${name}, ${date}, ${distNum}`;

				isSure = false;
				if (result.challengeResult) {
					const	content = new VRContent();
					document.getElementById('challengebadge').innerHTML = result.challengeResult.icon;
					document.getElementById('challengedistance').innerHTML = content.formatDistanceSum(result.challengeResult.distance);
					document.getElementById('challengename').innerHTML = VRContent.escapeHTML(result.challengeResult.name);
					document.getElementById('challengemedal').style.display = 'block';
				}

				loadContent(name, true);
			} else {
				if (result.nomember) {
					document.getElementById('accessdenied').style.display = 'block';
				} else if (result.doublewarning) {
					document.getElementById('doublewarning').style.display = 'block';
					isSure = true;
				} else if (result.tooearly) {
					showAlert(TooEarlyAlert);
					document.getElementById('datePicker').style.backgroundColor = '#FFDDCC';
					document.getElementById('datePicker').focus();
				} else if (result.toolate) {
					showAlert(TooLateAlert);
					document.getElementById('datePicker').style.backgroundColor = '#FFDDCC';
					document.getElementById('datePicker').focus();
				} else {
					document.getElementById('submiterror').style.display = 'block';
				}
				distEdit.value = distance;
				return false;
			}
		})
		.catch(err => {
			console.error(err);
			document.getElementById('submitting').style.display = 'none';
			document.getElementById('submiterror').style.display = 'block';
			document.getElementById('submitsuccess').style.display = 'none';
			distEdit.value = distance;
			return false;
		});

	if (typeof secondaryRoot !== 'undefined' && secondaryRoot.length > 0) {
		fetch(AppEngineURL + secondaryRoot + '/add', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({ alias: name, distance: distValue, date, membersonly: true })
			})
			.then(res => {
				console.log(res.text);
			})
			.catch(err => {
				console.error(err);
			});
	}

	return false;
}


function printPaper()
{
	const	name = document.getElementById('nameEdit').value
		.trim()
		.replace(new RegExp('{', 'g'), '[')
		.replace(new RegExp('}', 'g'), ']')
		.replace(new RegExp('_', 'g'), '-')
		.replace(new RegExp('/', 'g'), '-');

	if (!name || name.length < 1)
		return false;

	let		url = new URL(baseURL + '/print');
	url.searchParams.append('user', name);
	window.open(url);

	return false;
}


document.addEventListener('DOMContentLoaded', onInit);
