/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const	rootURL = '';
const	baseURL = AppEngineURL + rootURL;
let		auth = '';
let		confirmModal;
let		confirmModalBS;
let		userBgColor = '#ffff00';
let		userFgColor = '#ff0000';


function escapeHTML(s)
{
	const	lookup = {
		'&': "&amp;",
		'"': "&quot;",
		'\'': "&apos;",
		'<': "&lt;",
		'>': "&gt;"
	};
	return s.replace(/[&"'<>]/g, c => lookup[c] );
}


function showAlert(id, text)
{
	document.getElementById(id).style.display = 'block';
	document.getElementById(id + 'text').innerHTML = text.replace(/\n/g, '<br>');
}


async function makeRequest(body)
{
	document.getElementById('spinner').style.display = 'block';
	document.getElementById('rpcerror').style.display = 'none';
	document.getElementById('rpcsuccess').style.display = 'none';

	let		result;
	try
	{
		body.auth = auth;

		const	res = await fetch(baseURL + '/admin', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(body)
		});

		if (res.ok) {
			result = await res.json();
			if (!result.success)
				showAlert('rpcerror', result.errmsg);
		} else {
			const	txt = await res.text();
			showAlert('rpcerror', `${res.status} ${txt ?? res.statusText}`);
		}
	}
	catch(err)
	{
		showAlert('rpcerror', err);
	}
	document.getElementById('spinner').style.display = 'none';

	return result;
}


function editActivityCancel()
{
	document.getElementById('activityprops').style.display = 'none';
	document.getElementById('activitylist').style.display = 'block';
}


async function editActivitySave()
{
	const	distance = parseInt(document.getElementById('distanceEdit').value.trim());
	if (isNaN(distance)) {
		document.getElementById('distanceEdit').focus();
		return;
	}

	const	details = {
		user: document.getElementById('userEdit').value,
		distance,
		date: document.getElementById('datePicker').value,
		submit: parseInt(document.getElementById('submitEdit').value.split(' ')[0])
	};

	const	result = await makeRequest({
		command: 'set',
		template: document.getElementById('templateMenu').innerHTML,
		kind: 'activity',
		key: document.getElementById('activitykey').value,
		data: details
	});

	if (result && result.success) {
		await fillActivities(details.user)
		document.getElementById('activityprops').style.display = 'none';
		document.getElementById('activitylist').style.display = 'block';
		showAlert('rpcsuccess', result.msg);
	}
}


async function editActivity(b)
{
	const	data = b.getAttribute('data-vraui-activity');
	const	activity = JSON.parse(data);

	document.getElementById('activitykey').value = activity.key;
	document.getElementById('userEdit').value = activity.user;
	document.getElementById('datePicker').value = activity.date;
	document.getElementById('distanceEdit').value = activity.distance;
	document.getElementById('submitEdit').value = activity.submit.toString() + ' (' + new Date(activity.submit).toISOString() + ')';

	document.getElementById('activitylist').style.display = 'none';
	document.getElementById('activityprops').style.display = 'block';
}


async function deleteActivity(key)
{
	if (confirmModalBS)
		confirmModalBS.hide();

	const	result = await makeRequest({
		command: 'delete',
		template: document.getElementById('templateMenu').innerHTML,
		kind: 'activity',
		key
	});

	if (result && result.success) {
		const	row = document.getElementById(`activity_${key}`);
		row.parentNode.removeChild(row);
		showAlert('rpcsuccess', result.msg);
	}
}


function addActivityRow(table, act)
{
	let		row = table.insertRow(-1);
	row.id = `activity_${act.key}`;

	let		cell = row.insertCell();
	cell.innerHTML = escapeHTML(act.user);

	cell = row.insertCell();
	cell.innerHTML = act.date;

	cell = row.insertCell();
	cell.innerHTML = act.distance;

	cell = row.insertCell();
	cell.innerHTML = new Date(act.submit).toISOString();

	cell = row.insertCell();
	let	button = document.createElement('button');
	button.className = 'btn btn-sm btn-outline-info';
	button.setAttribute('type', 'button');
	button.setAttribute('onClick', `editActivity(this);`);
	button.setAttribute('data-vraui-activity', JSON.stringify(act));
	button.innerHTML = 'Edit';
	cell.append(button);

	cell = row.insertCell();
	button = document.createElement('button');
	button.className = 'btn btn-sm btn-outline-danger';
	button.setAttribute('type', 'button');
	button.setAttribute('data-bs-toggle', 'modal');
	button.setAttribute('data-bs-target', '#confirmModal');
	button.setAttribute('data-vraui-activity', act.key);
	button.innerHTML = 'Delete';
	cell.append(button);

	return row;
}


async function fillActivities(user)
{
	document.getElementById('userlist').style.display = 'none';

	const	result = await makeRequest({
		command: 'list',
		template: document.getElementById('templateMenu').innerHTML,
		kind: 'activity',
		user
	});

	const	t = document.getElementById('tbody_activities');
	t.innerHTML = '';
	if (result && result.items) {
		const	items = result.items.map(i => {
			const	u = i.data;
			u.key = i.key[1];
			return u;
		});
		items.sort((a, b) => {
			if (a.date < b.date)
				return -1;
			else if (a.date > b.date)
				return 1;
			return a.submit - b.submit;
		});

		for (const i of items) {
			addActivityRow(t, i);
		}
	}
	document.getElementById('activitylist').style.display = 'block';
}


function editUserCancel()
{
	document.getElementById('userprops').style.display = 'none';
	document.getElementById('userlist').style.display = 'block';
}


async function editUserSave()
{
	const	details = {
		bgcolor: userBgColor,
		fgcolor: userFgColor
	};

	const	official = document.getElementById('officialEdit').value.trim();
	if (official)
		details.official = official;

	const	svg = getMarkerSVG();
	if (svg)
		details.svg = svg;

	const	team = parseInt(document.getElementById('teamEdit').value.trim());
	if (!isNaN(team))
		details.team = team;

	const	result = await makeRequest({
		command: 'set',
		template: document.getElementById('templateMenu').innerHTML,
		kind: 'user',
		key: document.getElementById('nameEdit').value,
		data: details
	});

	if (result && result.success) {
		await fillUsers();
		document.getElementById('userprops').style.display = 'none';
		document.getElementById('userlist').style.display = 'block';
		showAlert('rpcsuccess', result.msg);
	}
}


function onChangeBg(ctrl)
{
	userBgColor = ctrl.value;
	setMarkers(document.getElementById('nameEdit').value, userBgColor, userFgColor);
}


function onChangeFg(ctrl)
{
	userFgColor = ctrl.value;
	setMarkers(document.getElementById('nameEdit').value, userBgColor, userFgColor);
}


async function editUser(b)
{
	const	data = b.getAttribute('data-vraui-user');
	const	user = JSON.parse(data);

	document.getElementById('nameEdit').value = user.name;
	if (user.official)
		document.getElementById('officialEdit').value = user.official;
	userBgColor = user.bgcolor;
	userFgColor = user.fgcolor;
	document.getElementById('bgcolor').value = userBgColor;
	document.getElementById('fgcolor').value = userFgColor;
	if (user.team)
		document.getElementById('teamEdit').value = user.team;
	setMarkers(user.name, userBgColor, userFgColor);
	selectMarker(user.svg);

	document.getElementById('userlist').style.display = 'none';
	document.getElementById('userprops').style.display = 'block';
}


async function deleteUser(user)
{
	if (confirmModalBS)
		confirmModalBS.hide();

	const	result = await makeRequest({
		command: 'delete',
		template: document.getElementById('templateMenu').innerHTML,
		kind: 'user',
		key: user
	});

	if (result && result.success) {
		const	row = document.getElementById(`user_${user}`);
		row.parentNode.removeChild(row);
		showAlert('rpcsuccess', result.msg);
	}
}


function addUserRow(table, user)
{
	let		row = table.insertRow(-1);
	row.id = `user_${user.name}`;

	let		cell = row.insertCell();
	cell.innerHTML = escapeHTML(user.name);

	let	svg;
	if (user.svg) {
		svg = user.svg
			.replace(new RegExp('{{size}}', 'g'), '36')
			.replace(new RegExp('{{bgcolor}}', 'g'), `${user.bgcolor}`)
			.replace(new RegExp('{{fgcolor}}', 'g'), `${user.fgcolor}`);
	} else {
		svg = `<svg width="36" height="36" viewBox="0 0 32 32">` +
			`<use href="#mapmarker-bg" style="fill:${user.bgcolor};"/>` +
			`<use href="#mapmarker"/>` +
			`<use href="#mapmarker-fg" style="fill:${user.fgcolor};"/>` +
			`</svg>`;
	}
	cell = row.insertCell();
	cell.innerHTML = svg;

	cell = row.insertCell();
	if (user.official)
		cell.innerHTML = escapeHTML(user.official);

	cell = row.insertCell();
	if (user.team)
		cell.innerHTML = user.team.toString();

	cell = row.insertCell();
	let	button = document.createElement('button');
	button.className = 'btn btn-sm btn-outline-info';
	button.setAttribute('type', 'button');
	button.setAttribute('onClick', `editUser(this);`);
	button.setAttribute('data-vraui-user', JSON.stringify(user));
	button.innerHTML = 'Edit';
	cell.append(button);

	cell = row.insertCell();
	button = document.createElement('button');
	button.className = 'btn btn-sm btn-outline-info';
	button.setAttribute('type', 'button');
	button.setAttribute('onClick', `fillActivities("${user.name}");`);
	button.innerHTML = 'Activities';
	cell.append(button);

	cell = row.insertCell();
	button = document.createElement('button');
	button.className = 'btn btn-sm btn-outline-danger';
	button.setAttribute('type', 'button');
	button.setAttribute('data-bs-toggle', 'modal');
	button.setAttribute('data-bs-target', '#confirmModal');
	button.setAttribute('data-vraui-user', user.name);
	button.innerHTML = 'Delete';
	cell.append(button);

	return row;
}


async function fillUsers()
{
	const	result = await makeRequest({
		command: 'list',
		template: document.getElementById('templateMenu').innerHTML,
		kind: 'user'
	});

	const	t = document.getElementById('tbody_users');
	t.innerHTML = '';
	if (result && result.items) {
		const	items = result.items.map(i => {
			const	u = i.data;
			u.name = i.key[1];
			return u;
		});
		items.sort((a, b) => a.name.localeCompare(b.name));

		for (const i of items) {
			addUserRow(t, i);
		}
	}
	document.getElementById('userlist').style.display = 'block';
}


async function setActiveTemplate(e)
{
	const	b = document.getElementById('templateMenu');
	b.innerHTML = e.innerHTML;

	document.getElementById('activitylist').style.display = 'none';
	document.getElementById('activityprops').style.display = 'none';
	document.getElementById('userprops').style.display = 'none';

	await fillUsers();
}


async function onExport()
{
	const	template = document.getElementById('templateMenu').innerHTML;
	const	result = await makeRequest({
		command: 'export',
		template
	});

	if (result) {
		const	file = new Blob([JSON.stringify(result, null, 2)], { type: 'application/json' });
		const	a = document.createElement('a');
		const	url = URL.createObjectURL(file);
		a.href = url;
		a.download = `${template}_${new Date().getFullYear()}-${(new Date().getMonth() + 1).toString().padStart(2, '0')}-${(new Date().getDate()).toString().padStart(2, '0')}.json`;
		a.click();
		URL.revokeObjectURL(url);
	}
}


async function onSaveActivities()
{
	const	template = document.getElementById('templateMenu').innerHTML;
	const	result = await makeRequest({
		command: 'list',
		template,
		kind: 'activity'
	});

	if (result && result.items) {
		const	resultRows = result.items.map(i => `${i.data.user}\t${i.data.date}\t${i.data.distance}\t${new Date(i.data.submit ?? Date.now()).toISOString()}`);
		resultRows.sort((a, b) => a.localeCompare(b));
		const	file = new Blob([resultRows.join('\n')], { type: 'text/csv' });
		const	a = document.createElement('a');
		const	url = URL.createObjectURL(file);
		a.href = url;
		a.download = `Activities_${template}_${new Date().getFullYear()}-${(new Date().getMonth() + 1).toString().padStart(2, '0')}-${(new Date().getDate()).toString().padStart(2, '0')}.csv`;
		a.click();
		URL.revokeObjectURL(url);
	}
}


async function importActivities(activities)
{
	const	template = document.getElementById('templateMenu').innerHTML;

	try
	{
		const	items = [];

		const	rows = activities.split('\n').map(i => i.trim()).filter(i => (i));
		for (const row of rows) {
			const	cols = row.split(/[\t;]/).map(s => {
				s = s.trim();
				if (s.length > 0 && s[0] === '"')
					s = s.slice(1, -1);
				return s.trim();
			});
			if (cols.length < 3) {
				console.warn(`Invalid row: "${row}"`);
				continue;
			}

			const	act = {
				user: cols[0],
				date: cols[1],
				distance: parseInt(cols[2])
			};
			if (isNaN(new Date(act.date).getTime())) {
				console.warn(`Invalid date: "${cols[1]}"`);
				continue;
			}
			if (!Number.isInteger(act.distance)) {
				console.warn(`Invalid distance: "${cols[2]}"`);
				continue;
			}

			if (cols.length > 3) {
				act.submit = new Date(cols[3]).getTime();
				if (isNaN(act.submit)) {
					act.submit = parseInt(cols[3]);
					if (isNaN(act.submit)) {
						console.warn(`Invalid submit timestamp: "${cols[3]}"`);
						continue;
					}
				}
			}

			items.push(act);
		}
		if (items.length === 0) {
			showAlert('rpcerror', 'There were no valid records to import. See Console for details.');
			return;
		}

		let		ts = Date.now() - 1;
		items.reverse();
		for (const item of items) {
			if (!item.submit) {
				item.submit = ts;
				--ts;
			}
		}
		items.reverse();

		const	result = await makeRequest({
			command: 'addactivities',
			template,
			data: items
		});
		if (result && result.success) {
			showAlert('rpcsuccess', result.msg);

			if (document.getElementById('activitylist').style.display === 'block') {
				await fillUsers();
			}
		}
	}
	catch(err)
	{
		showAlert('rpcerror', err);
	}
}


async function onLoadActivities()
{
	const	input = document.createElement('input');
	input.type = 'file';
	input.accept = 'text/csv,.csv,.txt';

	input.onchange = e => { 
		const	file = e.target.files[0]; 
		const	reader = new FileReader();
		reader.readAsText(file,'UTF-8');

		reader.onload = readerEvent => importActivities(readerEvent.target.result);
	}

	input.click();
}


async function fillTemplateDropdown()
{
	const	result = await makeRequest({
		command: 'listtemplates'
	});

	const	m = document.getElementById('templateItems');
	m.innerHTML = '';
	if (!result || !result.success)
		return;

	let		b;
	for (const t of result.templates) {
		const	li = document.createElement('li');
		b = document.createElement('button');
		b.className = 'dropdown-item';
		b.setAttribute('type', 'button');
		b.setAttribute('onClick', 'setActiveTemplate(this);');
		b.innerHTML = t;
		li.append(b);
		m.append(li);
	}

	if (b) {
		await setActiveTemplate(b);
	}
}


function onLogin()
{
	auth = document.getElementById('auth').value.trim();
	if (!auth) {
		document.getElementById('auth').focus();
		return;
	}

	document.getElementById('loginForm').style.display = 'none';
	document.getElementById('mainPage').style.display = 'block';

	fillTemplateDropdown();

	confirmModal = document.getElementById('confirmModal');
	confirmModal.addEventListener('show.bs.modal', function (event) {
		const	button = event.relatedTarget;
		const	key = button.getAttribute('data-vraui-activity');
		if (key) {
			document.getElementById('confirmModalContent').innerHTML = `Are you sure to delete activity "${key}"?`;
			document.getElementById('confirmModalButton').setAttribute('onClick', `deleteActivity("${key}");`);
		} else {
			const	user = button.getAttribute('data-vraui-user');
			if (user) {
				document.getElementById('confirmModalContent').innerHTML = `Are you sure to delete user "${user}"?`;
				document.getElementById('confirmModalButton').setAttribute('onClick', `deleteUser("${user}");`);
			}
		}
	});
	confirmModalBS = new bootstrap.Modal('#confirmModal', {});	  
}


function findGetParameter(parameterName)
{
	const	items = location.search.substr(1).split("&");
	for (var index = 0; index < items.length; index++) {
		const	tmp = items[index].split("=");
		if (tmp[0] === parameterName)
			return decodeURIComponent(tmp[1]);
	}
}


function onInit()
{
	const	authParam = findGetParameter('auth');
	if (authParam) {
		document.getElementById('auth').value = authParam;
		onLogin();
	}
}


document.addEventListener('DOMContentLoaded', onInit);
