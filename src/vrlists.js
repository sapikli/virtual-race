/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

// Virtual race activity-based list generation
class VRLists {
	constructor()
	{
		const	self = this;
	}

	static #sumByUser(activities)
	{
		let		sums = {};
		for (const act of activities) {
			if (!sums[act.user])
				sums[act.user] = { distance: 0, count: 0, challengeResults: [] };
			sums[act.user].distance += act.distance;
			++sums[act.user].count;
			if (act.challengeResult)
				sums[act.user].challengeResults.push(act.challengeResult);
		}

		return Object.entries(sums).map(i => {
			return { user: i[0], distance: i[1].distance, count: i[1].count, challengeResults: i[1].challengeResults };
		});
	}

	static #getLatest(activities)
	{
		activities.sort((a, b) => (b.submit - a.submit));

		let		latest = [];
		let		i;
		for (i = 0; i < 3 && i < activities.length; ++i) {
			latest.push({
				user: activities[i].user,
				distance: activities[i].distance,
				date: activities[i].date,
				challengeResult: activities[i].challengeResult
			});
		}

		return latest;
	}

	static #getLongest(activities)
	{
		activities.sort((a, b) => {
			if (a.distance !== b.distance)
				return b.distance - a.distance;
			return a.submit - b.submit;
		});

		let		longest = [];
		let		i;
		for (i = 0; i < 3 && i < activities.length; ++i) {
			longest.push({
				user: activities[i].user,
				distance: activities[i].distance,
				date: activities[i].date
			});
		}

		return longest;
	}

	static #getList(activities)
	{
		activities.sort((a, b) => {
			if (a.date !== b.date)
				return (a.date > b.date ? 1 : -1);
			if (a.count !== b.count)
				return (a.count - b.count);
			return a.submit - b.submit;
		});

		const	byUser = VRLists.#sumByUser(activities);

		return byUser;
	}

	static #getMine(activities, user)
	{
		const	byDate = activities.filter(item => item.user === user);
		byDate.sort((a, b) => {
			if (a.date !== b.date)
				return (a.date > b.date ? 1 : -1);
			return a.submit - b.submit;
		});

		let		result = [];
		let		total = 0;
		for (const act of byDate) {
			total += act.distance;
			result.push({
				distance: act.distance,
				total: total,
				date: act.date,
				challengeResult: act.challengeResult
			});
		}

		return result;
	}

	static createContent(activities, filteredActivities, user)
	{
		let		result = {};
		if (activities.length > 0) {
			result.latest = VRLists.#getLatest(activities);
			result.longest = VRLists.#getLongest(filteredActivities);
			result.list = VRLists.#getList(activities);
			result.mine = VRLists.#getMine(activities, user);
		}

		return result;
	}
}


module.exports = VRLists;
