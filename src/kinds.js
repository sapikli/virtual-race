/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const	Kinds = Object.freeze({
	Activity:	'activity',
	Challenge:	'challenge',
	Target:		'target',
	User:		'user'
});

const	AllKinds = [ Kinds.Activity, Kinds.Challenge, Kinds.Target, Kinds.User ];


module.exports = {
	Kinds,
	AllKinds
}
