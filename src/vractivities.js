/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const logger = require('./logger');
const crc = require('crc');
const { Datastore } = require('@google-cloud/datastore');
const { Kinds } = require('./kinds');
const VRChallenges = require('./vrchallenges');


const	instanceId = crc.crc32(Buffer.from(process.env.GAE_INSTANCE ?? '', 'utf8')).toString(16).padStart(8, '0');


// Temporary, for dummies who entered multiple activities as a single one
const	dummies = [
	'1705578482231_e6ab_ec5c790c',
	'1705666051112_bae6_ce316a56',
	'1705666472848_523d_ce316a56',
	'1705669527214_f9c7_ce316a56',
	'1705695896719_7d3e_b9ab4aa8',
	'1705697493180_63e5_b9ab4aa8',
	'1706015456231_f1ec_0f5c758a'
];


// Virtual race activity functionality
class VRActivities {
	#datastore;
	#template;
	#lastAdded;
	#vrChallenges;

	constructor(datastore, template)
	{
		const	self = this;

		self.#datastore = datastore;
		self.#template = template;
		self.#lastAdded = {};
		self.#vrChallenges = new VRChallenges(datastore, template);
	}

	getDatastoreObject(params)
	{
		const	self = this;

		const	user = (params.user ?? params.name);
		const	submit = (Number.isSafeInteger(params.submit) ? params.submit : Date.now());
		const	userId = crc.crc16ccitt(Buffer.from(user, 'utf8')).toString(16).padStart(4, '0');

		return {
			key: self.#datastore.key({
				namespace: self.#template,
				path: [Kinds.Activity, `${submit}_${userId}_${instanceId}`]
			}),
			data: {
				user,
				date: params.date,
				distance: parseInt(params.distance),
				submit
			}
		};
	}

	async add(params)
	{
		const	self = this;

		let		result = {
			success: false
		};

		if (params.name && params.distance && Number.isSafeInteger(parseInt(params.distance)) && params.date) {
			result.user = params.name;

			const	act = self.getDatastoreObject(params);
			try {
				await	self.#datastore.save(act);
				self.#lastAdded[params.name] = act.data;
				result.success = true;

				await	self.#vrChallenges.init(false);

				const	challengeResult = self.#vrChallenges.get(act.data.distance);
				if (challengeResult)
					result.challengeResult = challengeResult;
			}
			catch(err) {
				logger.error('Failed to store: ', err);
				result.errormsg = 'Failed to store.';
			}
		} else {
			result.errormsg = 'Missing form parameters.';
		}

		return result;
	}

	static filterDummies(activities)
	{
		if (!activities)
			return activities;
		
		return activities.filter(act => !dummies.includes(act[Datastore.KEY].name));
	}
	
	async get()
	{
		const	self = this;
		let		result = [];

		try {
			const	query = self.#datastore.createQuery(self.#template, Kinds.Activity);
			[result] = await self.#datastore.runQuery(query);

			// Recently added entries may not be returned by runQuery yet.
			for (const [key, value] of Object.entries(self.#lastAdded)) {
				let		found = false;
				for (const act of result) {
					if (act.submit === value.submit && act.user === value.user && act.date === value.date && act.distance === value.distance) {
						found = true;
						break;
					}
				}
				if (found)
					delete self.#lastAdded[key];
				else
					result.push(value);
			}

			await	self.#vrChallenges.init(false);
			
			for (let act of result) {
				act.distance = parseInt(act.distance);

				if (dummies.includes(act[Datastore.KEY].name))
					continue;

				const	challengeResult = self.#vrChallenges.get(act.distance);
				if (challengeResult)
					act.challengeResult = challengeResult;
			}
		} catch(err) {
			logger.error('Error querying the Datastore: ', err);
		}

		result.sort((a, b) => {
			if (a.date !== b.date)
				return (a.date < b.date ? -1 : 1);
			return a.submit - b.submit;
		});

		return result;
	}

	async getLast(user)
	{
		const	self = this;
		let		result = [];

		try {
			const	userId = crc.crc16ccitt(Buffer.from(user, 'utf8')).toString(16).padStart(4, '0');
			const	query = self.#datastore.createQuery(self.#template, Kinds.Activity);
			const	[allkeys] = await self.#datastore.runQuery(query.select('__key__'));
			const	userkeys = allkeys
				.map(item => item[Datastore.KEY])
				.filter(item => {
					const	parts = item.name.split('_');
					return (parts && parts.length === 3 && parts[1] === userId);
				});
			
			if (userkeys.length > 0) {
				const	[getresult] = await self.#datastore.get(userkeys);
				result = getresult.filter(item => item.user === user);	// The above user hash may leave other users.
			}

			// Recently added entries may not be returned by runQuery yet.
			for (const [key, value] of Object.entries(self.#lastAdded)) {
				let		found = false;
				for (const act of result) {
					if (act.submit === value.submit && act.user === value.user && act.date === value.date && act.distance === value.distance) {
						found = true;
						break;
					}
				}
				if (found)
					delete self.#lastAdded[key];
				else
					result.push(value);
			}
		} catch(err) {
			logger.error('Error querying the Datastore: ', err);
		}
		if (result.length === 0)
			return null;

		result.sort((a, b) => {
			return b.submit - a.submit;
		});

		return result[0];
	}

	async reInit()
	{
		const	self = this;

		await	self.#vrChallenges.init(true);
	}
}


module.exports = VRActivities;
