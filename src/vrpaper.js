/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const fs = require('fs');
const path = require('path');

const logger = require('./logger');
const { fromWGS84ToGM, TrackInterpolator } = require('./trackinterpolator');


// Virtual race participation paper functionality
class VRPaper {
	#templatePath;
	#gmTrack;
	#interpolator;
	#config;
	#svg;

	constructor(templatePath, gmTrack, interpolator)
	{
		const	self = this;

		self.#templatePath = templatePath;
		self.#gmTrack = gmTrack;
		self.#interpolator = interpolator;
		try {
			self.#config = JSON.parse(fs.readFileSync(path.join(templatePath, 'paper.json')));
			self.#svg = fs.readFileSync(path.join(templatePath, 'paper.svg')).toString();
		}
		catch(err)
		{
			if (err.code !== 'ENOENT')
				logger.error(`${templatePath}: ${err.message}`);
		}
	}

	isValid()
	{
		const	self = this;

		if (!self.#config || !self.#svg || self.#svg.length === 0)
			return false;

		if (self.#config.width < 100 || self.#config.height < 100) {
			logger.warn(`Invalid ${self.#templatePath}: Invalid dimensions`);
			return false;
		}
		if (self.#config.hasOwnProperty('mapscale')) {
			if (self.#config.mapscale < 0.1 || self.#config.mapscale > 1.0) {
				logger.warn(`Invalid ${self.#templatePath}: Invalid map scale`);
				return false;
			}
			if (self.#config.hasOwnProperty('mapcenterx') && (self.#config.mapcenterx < 50 || self.#config.mapcenterx > self.#config.width - 50)) {
				logger.warn(`Invalid ${self.#templatePath}: Invalid map center X`);
				return false;
			}
			if (self.#config.hasOwnProperty('mapcentery') && (self.#config.mapcentery < 50 || self.#config.mapcentery > self.#config.height - 50)) {
				logger.warn(`Invalid ${self.#templatePath}: Invalid map center Y`);
				return false;
			}
			if (typeof self.#config.trackpoint !== 'string' || self.#config.trackpoint.indexOf('{{x}}') < 0 || self.#config.trackpoint.indexOf('{{y}}') < 0) {
				logger.warn(`Invalid ${self.#templatePath}: Invalid 'trackpoint'`);
				return false;
			}
			if (!self.#gmTrack || !self.#gmTrack.track || self.#gmTrack.track.length < 2 || !self.#interpolator) {
				logger.warn(`Invalid ${self.#templatePath}: Missing track`);
				return false;
			}
		}
		if (!Array.isArray(self.#config.places) || self.#config.places.length === 0) {
			logger.warn(`Invalid ${self.#templatePath}: Missing 'places'`);
			return false;
		}
		if (!Array.isArray(self.#config.medals) || self.#config.medals.length !== 3) {
			logger.warn(`Invalid ${self.#templatePath}: Missing 'medals'`);
			return false;
		}
		if (self.#config.hasOwnProperty('hardwork')) {
			if ((typeof self.#config.hardwork !== 'object') || !self.#config.hardwork.hasOwnProperty('text') || (typeof self.#config.hardwork.text !== 'string')) {
				logger.warn(`Invalid ${self.#templatePath}: Invalid 'hardwork'`);
				return false;
			}
			if (self.#config.hardwork.hasOwnProperty('icon') && typeof self.#config.hardwork.icon !== 'string') {
				logger.warn(`Invalid ${self.#templatePath}: Invalid 'hardwork'`);
				return false;
			}
		}
		if (!self.#config.hasOwnProperty('badges') || (typeof self.#config.badges !== 'object') || !self.#config.badges.hasOwnProperty('row')) {
			logger.warn(`Invalid ${self.#templatePath}: Missing 'badges'`);
			return false;
		}
			
		return true;
	}

	#generateMap(mine, svg)
	{
		const	self = this;

		const	mapCenterX = self.#config.mapcenterx ?? self.#config.width * 0.5;
		const	mapCenterY = self.#config.mapcentery ?? self.#config.height * 0.5;
		const	bounds = self.#gmTrack.bounds;
		const	centerX = (bounds[0].x + bounds[1].x) / 2;
		const	centerY = (bounds[0].y + bounds[1].y) / 2;
		const	trackWidth = bounds[1].x - bounds[0].x;
		const	trackHeight = bounds[1].y - bounds[0].y;
		const	scale = Math.min(self.#config.width / trackWidth, self.#config.height / trackHeight) * self.#config.mapscale;

		let		x;
		let		y;
		let		lastX = -1;
		let		lastY = -1;
		let		i;
		let		hadL = false;
		let		line = '';
		const	track = self.#gmTrack.track;
		let		trackPath = '';
		for (i = 0; i < track.length; ++i) {
			x = Math.round(mapCenterX + scale * (track[i].x - centerX));
			y = self.#config.height - Math.round(mapCenterY + scale * (track[i].y - centerY));
			if (Math.abs(x - lastX) < 5 && Math.abs(y - lastY) < 5)
				continue;

			if (i === 0) {
				line += `M${x.toFixed()} ${y.toFixed()}`;
			} else if (hadL) {
				line += ` ${(x - lastX).toFixed()} ${(y - lastY).toFixed()}`;
			} else {
				line += `l${(x - lastX).toFixed()} ${(y - lastY).toFixed()}`;
				hadL = true;
			}

			lastX = x;
			lastY = y;
			if (line.length >= 80) {
				trackPath += line + '\n';
				line = '';
			}
		}
		if (line.length > 0)
			trackPath += line + '\n';

		const	srcx = new RegExp('{{x}}', 'g');
		const	srcy = new RegExp('{{y}}', 'g');
		let		points = '';
		for (let item of mine) {
			if (item.total < self.#interpolator.getLength()) {
				const	pt = self.#interpolator.getCoordFromDist(item.total);
				const	ptGM = fromWGS84ToGM(pt[0], pt[1])
				x = mapCenterX + scale * (ptGM.x - centerX);
				y = self.#config.height - (mapCenterY + scale * (ptGM.y - centerY));
				points += '\t' + self.#config.trackpoint.replace(srcx, x.toFixed()).replace(srcy, y.toFixed()) + '\n';
			}
		}
	
		const	startX = (mapCenterX + scale * (track[0].x - centerX)).toFixed();
		const	startY = (self.#config.height - (mapCenterY + scale * (track[0].y - centerY))).toFixed();
		const	finishX = (mapCenterX + scale * (track.at(-1).x - centerX)).toFixed();
		const	finishY = (self.#config.height - (mapCenterY + scale * (track.at(-1).y - centerY))).toFixed();

		return svg
			.replace(new RegExp('{{track}}', 'g'), trackPath)
			.replace(new RegExp('{{points}}', 'g'), points)
			.replace(new RegExp('{{startx}}', 'g'), startX)
			.replace(new RegExp('{{starty}}', 'g'), startY)
			.replace(new RegExp('{{finishx}}', 'g'), finishX)
			.replace(new RegExp('{{finishy}}', 'g'), finishY);
	}

	static #getPrintedLength(str)
	{
		let len = 0;
		let i;
		for (i = 0; i < str.length; ) {
			const	pos1 = str.indexOf('&', i);
			if (pos1 < 0) {
				len += (str.length - i);
				break;
			}

			const	pos2 = str.indexOf(';', pos1 + 1);
			if (pos2 < 0 || pos2 < pos1 + 2) {
				logger.error(`Incorrect HTML entity in ${str}`);
				break;
			}

			len += (pos1 - i);
			if (str[pos1 + 1] === '#') {
				let		code;
				if (str[pos1 + 2] === 'x')
					code = Number.parseInt(str.slice(pos1 + 3, pos2), 16);
				else
					code = Number.parseInt(str.slice(pos1 + 2, pos2));

				// Rough estimate of Unicode emojis (treated as 2-character wide)
				if (code >= 0x1000)
					++len;
			}

			++len;
			i = pos2 + 1;
		}

		return len;
	}

	static #breakString(str, start, end, offs)
	{
		let		i = start;
		if (offs && offs > 0)
			i += offs;

		for ( ; i < end; ++i) {
			if (str[i] === ' ')
				return [
					str.slice(0, i).trim(),
					str.slice(i + 1).trim()
				];
		}

		return null;
	}

	static #breakAtMiddle(str)
	{
		const	printedLen = VRPaper.#getPrintedLength(str);
		let		len = 0;
		let		i;
		for (i = 0; i < str.length; ) {
			const	pos1 = str.indexOf('&', i);
			if (pos1 < 0)
				return VRPaper.#breakString(str, i, str.length, Math.floor(printedLen / 2) - len);

			const	pos2 = str.indexOf(';', pos1 + 1);
			if (pos2 < 0 || pos2 < pos1 + 2) {
				logger.error(`Incorrect HTML entity in ${str}`);
				break;
			}

			if (len + (pos1 - i) > printedLen / 2) {
				const	ret = VRPaper.#breakString(str, i, pos1, Math.floor(printedLen / 2) - len);
				if (ret)
					return ret;
			}

			len += (pos1 - i);
			if (str[pos1 + 1] === '#') {
				let		code;
				if (str[pos1 + 2] === 'x')
					code = Number.parseInt(str.slice(pos1 + 3, pos2), 16);
				else
					code = Number.parseInt(str.slice(pos1 + 2, pos2));

				// Rough estimate of Unicode emojis (treated as 2-character wide)
				if (code >= 0x1000)
					++len;
			}

			++len;
			i = pos2 + 1;
		}

		return [ str, '' ];
	}

	static #putTSpan(coords, text)
	{
		if (text[0] === '[')
			return `\t<tspan ${coords}>\xA0</tspan><tspan class="remarks">${text}</tspan>\n`;

		return `\t<tspan ${coords}>` + text.replace(/\[/g, '</tspan><tspan class="remarks">[') + '</tspan>\n';
	}

	#formatBadges(badges)
	{
		const	self = this;

		let		result = '';
		for (const badge of badges) {
			let	text = '';
			if (badge.icon)
				text = badge.icon + ' ';
			text += badge.text;

			let		pos = text.indexOf('\n');
			let		descr;
			if (pos > 0) {
				descr = [ text.slice(0, pos), text.slice(pos + 1) ];
			} else if (VRPaper.#getPrintedLength(text) >= 40 && self.#config.badges.firstrow && self.#config.badges.secondrow) {
				pos = text.indexOf('[');
				if (pos >= 0 && pos > text.length / 4 && pos < 3 * text.length / 4)
					descr = VRPaper.#breakString(text, pos - 1, text.length);
				else
					descr = VRPaper.#breakAtMiddle(text);
			}

			if (descr) {
				if (self.#config.badges.upwards) {
					result += VRPaper.#putTSpan(self.#config.badges.secondrow, descr[1]);
					result += VRPaper.#putTSpan(self.#config.badges.firstrow, descr[0]);
				} else {
					result += VRPaper.#putTSpan(self.#config.badges.firstrow, descr[0]);
					result += VRPaper.#putTSpan(self.#config.badges.secondrow, descr[1]);
				}
			} else {
				result += VRPaper.#putTSpan(self.#config.badges.row, text);
			}
		}

		return result;
	}

	#generateMonthly(months)
	{
		const	self = this;

		let		maxmonth = 0;
		for (const m of Object.values(months)) {
			if (maxmonth < m.distance)
				maxmonth = m.distance;
		}

		const	config = self.#config.monthly;
		let		s = '';
		let		x = config.xstart;
		for (const [date, m] of Object.entries(months)) {
			s += `<svg x="${x}" y="${config.ystart}" width="${config.xoffs}" height="${config.barheight * 1.5}">\n`;
			const	h = config.barheight * m.distance / maxmonth;
			const	y = config.ybars - h;
			const	yt = y + config.yoffsmedals;
			s += `\t<rect x="${(config.xoffs - config.barwidth) / 2}" y="${y.toFixed(1)}" width="${config.barwidth}" height="${h.toFixed(1)}" style="${config.barstyle}"/>\n`;
			if (m.place >= 1 && m.place <= 3)
				s += `\t<text x="${config.xoffs / 2}" y="${yt.toFixed(1)}" class="default distance">${self.#config.medals[m.place - 1]}</text>\n`;
			s += `\t<text x="${config.xoffs / 2}" y="${config.ydistance}" class="default distance">${(m.distance / 1000).toFixed(1).replace(/\./g, ',')}</text>\n`;
			if (typeof config.monthtransform === 'string') {
				const	monthNum = parseInt(date.slice(5));
				let		month = monthNum.toString();
				if (config.romanmonths)
					month = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII'][month - 1] + '.';
				s += `\t<text transform="${config.monthtransform}" class="default month">${month}</text>\n`;
			}
			s += '</svg>\n';
			x += config.xoffs;
		}

		return s;
	}

	get(user, medals, targets, mine, months, longest, ishardest)
	{
		const	self = this;

		let		name = user.official ?? user.name;
		let		userfg = user.fgcolor ?? 'yellow';
		let		userbg = user.bgcolor ?? 'red';

		let		place = '';
		if (medals.place >= 1 && medals.place <= self.#config.places.length)
			place = self.#config.places[medals.place - 1];

		const	d1 = new Date(mine[0].date).getTime() / 1000;
		const	d2 = new Date(medals.finishdate).getTime() / 1000;
		const	days = Math.floor(1 + (d2 + 43200 - d1) / 86400).toString();

		let		count;
		for (count = 0; count < mine.length; ++count) {
			if (mine[count].date > medals.finishdate)
				break;
		}

		let		totaldistance = 0;
		for (const m of mine) {
			totaldistance += m.distance;
		}

		let		badges = [];
		if (Array.isArray(self.#config.longest)) {
			let	i;
			for (i = 0; i < self.#config.longest.length; ++i) {
				if (longest[i].user !== user.name)
					continue;

				const	km = Math.floor(longest[i].distance / 1000).toString();
				const	m = (longest[i].distance % 1000).toString().padStart(3, '0');
				badges.push({ icon: self.#config.longest[i].icon, text: self.#config.longest[i].text.replace(new RegExp('{{km}}', 'g'), km).replace(new RegExp('{{m}}', 'g'), m) });
				// Quit after the first match. Not enough place for multiple medals, sorry.
				break;
			}
		}

		for (const medal of medals.badges) {
			if (medal.hideonmine || medal.finish || !medal.place || medal.place < 1 || medal.place > 3)
				continue;

			let		badge = {
				icon: self.#config.medals[medal.place - 1],
				text: ''
			};
			for (const target of targets) {
				if (target.distance === medal.distance) {
					badge.text = target.name;
					break;
				}
			}

			badges.push(badge);
		}

		const	challenges = mine
			.filter(item => item.challengeResult)
			.map(item => item.challengeResult);
		let		ch = {};
		for (const res of challenges) {
			if (ch[res.distance])
				++ch[res.distance].count;
			else 
				ch[res.distance] = { count: 1, distance: res.distance, icon: res.icon, name: res.name };
		}

		const	arr = Object.entries(ch);
		arr.sort((a, b) => b[0] - a[0]);
		for (const res of arr) {
			let		medal = res[1].icon;
			if (res[1].count > 1)
				medal = `${res[1].count}&#xD7;` + medal;
			badges.push({ icon: medal, text: res[1].name });
		}

		if (ishardest && self.#config.hasOwnProperty('hardwork'))
			badges.push(self.#config.hardwork);

		if (self.#config.badges.upwards)
			badges.reverse();

		const	badgeStr = self.#formatBadges(badges);

		let		monthlyStr = '';
		if (self.#config.hasOwnProperty('monthly') && months)
			monthlyStr = self.#generateMonthly(months);

		const	totalMStr = (totaldistance % 1000).toString().padStart(3, '0');
		let		totalMTrimmed = (Math.floor(totaldistance / 100) % 10).toString();
		if ((Math.floor(totaldistance / 10) % 10) !== 0)
			totalMTrimmed += (Math.floor(totaldistance / 10) % 10).toString();
		if ((totaldistance % 10) !== 0)
			totalMTrimmed += (totaldistance % 10).toString();

		let		svg = self.#svg
			.replace(new RegExp('{{name}}', 'g'), name)
			.replace(new RegExp('{{userfg}}', 'g'), userfg)
			.replace(new RegExp('{{userbg}}', 'g'), userbg)
			.replace(new RegExp('{{place}}', 'g'), place)
			.replace(new RegExp('{{days}}', 'g'), days)
			.replace(new RegExp('{{count}}', 'g'), count.toString())
			.replace(new RegExp('{{totalcount}}', 'g'), mine.length.toString())
			.replace(new RegExp('{{totalkm}}', 'g'), Math.floor(totaldistance / 1000).toString())
			.replace(new RegExp('{{totalm}}', 'g'), totalMStr)
			.replace(new RegExp('{{totalmtrim}}', 'g'), totalMTrimmed)
			.replace(new RegExp('{{monthly}}', 'g'), monthlyStr)
			.replace(new RegExp('{{badges}}', 'g'), badgeStr);

		if (self.#config.hasOwnProperty('mapscale'))
			return self.#generateMap(mine, svg);
		else
			return svg;
	}
}


module.exports = VRPaper
