/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const winston = require('winston');
const path = require('path');
const { getPkgJsonDir } = require('./utils');


const projectDir = getPkgJsonDir();
const pkg = require(path.join(projectDir, 'package.json'));

const labelKey = 'logging.googleapis.com/labels';
const prefix = 'sapikli-app';


const validLogLevels = {
	error: {
		code: 0, 
		severity: 'ERROR'
	},
	warn: {
		code: 1, 
		severity: 'WARNING'
	}, 
	info: {
		code: 2, 
		severity: 'INFO'
	}, 
	http: {
		code: 3, 
		severity: 'DEFAULT'
	},
	verbose: {
		code: 4, 
		severity: 'DEFAULT'
	}, 
	debug: {
		code: 5, 
		severity: 'DEBUG'
	}, 
	silly: {
		code: 6, 
		severity: 'DEFAULT'
	}
};


const defMinLogLevel = (process.env.MIN_LOG_LEVEL in validLogLevels) ? process.env.MIN_LOG_LEVEL : 'info';


const cloudFormat = winston.format.combine(
	winston.format.timestamp({}), // ==> new Date().toISOString()
	winston.format.printf(function(info) {

		const { timestamp, level, labels, stack, ...remainder } = info

		let message = (typeof remainder.message === 'string') ? remainder.message : JSON.stringify(remainder);
		if (stack)
			message += '\n' + stack;

		const entry = {
			severity: (level in validLogLevels) ? validLogLevels[level].severity : 'DEFAULT',
			message: message,
			[labelKey] : {
				[`${prefix}/name`]: pkg.name,
				[`${prefix}/version`]: pkg.version
			}
		};
		if (labels) {
			for (const [key,val] of Object.entries(labels)) {
				Object.assign(entry[labelKey], { [`${prefix}/${key}`]: val });
			}
		}
		return JSON.stringify(entry);
	})
)

const localFormat = winston.format.combine(
	winston.format.timestamp({
		format: 'YYYY-MM-DD HH:mm:ss'
	}),
	winston.format.colorize({ level: true }),
	winston.format.printf(function(info) {
		const { timestamp, level, labels, stack, ...remainder } = info
		let ret = `${timestamp} ${level}: `;
		if (labels) {
			ret += '[';
			if (labels.module)
				ret += labels.module;
			if (labels.table) {
				if (labels.module)
					ret += '|';
				ret += labels.table;
			}
			ret += '] ';
		}

		ret += (typeof remainder.message === 'string') ? remainder.message : JSON.stringify(remainder);

		if (stack) {
			ret += '\n' + stack;
		}
		return ret;
	})
)


console.log(`Initialising logger for ${pkg.name} v${pkg.version}`);


const transports = [];
const consoleTransport = new winston.transports.Console({
	stderrLevels: ['error', 'warn'],
	format: (process.env.NODE_ENV === 'production') ? cloudFormat : localFormat
});
transports.push(consoleTransport);
	

const logger = winston.createLogger({
	level: defMinLogLevel,
	format: winston.format.json(),
	transports: transports
})


module.exports = logger;
