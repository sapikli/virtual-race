/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const logger = require('./logger');
const { Datastore } = require('@google-cloud/datastore');
const VRActivities = require('./vractivities');
const VRResults = require('./vrresults');
const VRUsers = require('./vrusers');
const { Kinds, AllKinds } = require('./kinds');


const	MaxOpsPerCall = 400;		// Datastore limit === 500.


// Virtual race admin functionality
class VRAdmin {
	#datastore;

	constructor(datastore)
	{
		const	self = this;

		self.#datastore = datastore;
	}

	static #getPathFromParams(params)
	{
		if (!params.key)
			return;

		if (typeof params.key === 'object' && params.key.hasOwnProperty('namespace') && params.key.hasOwnProperty('path')
			&& typeof params.key.namespace === 'string' && Array.isArray(params.key.path) && params.key.path.length === 2
			&& typeof params.key.path[0] === 'string' && typeof params.key.path[1] === 'string') {
			if (params.template !== params.key.namespace)
				return;
			// Full key
			return params.key.path;
		} else if (typeof params.key === 'string') {		
			if (params.kind && typeof params.kind === 'string') {
				// kind, key
				return [params.kind, params.key];
			} else {
				// 'kind_key' (legacy)
				const	pos = params.key.indexOf('_');
				if (pos > 0 && pos + 1 < params.key.length)
					return [params.key.slice(0, pos), params.key.slice(pos + 1)];
			}
		} else if (Array.isArray(params.path) && params.path.length === 2 && typeof params.path[0] === 'string' && typeof params.path[1] === 'string') {
			// [kind, key] path
			return [params.path[0], params.path[1]];
		}
	}

	async #clearAllActivities(templateName)
	{
		const	self = this;

		if (!templateName || templateName.length === 0)
			return { errmsg: 'Error: Missing template name.' };

		let		result = {};
		try {
			const	query = self.#datastore.createQuery(templateName, Kinds.Activity);
			const	[activities] = await self.#datastore.runQuery(query);
			const	keys = activities.map(item => item[Datastore.KEY]);

			let	startIdx;
			for (startIdx = 0; startIdx < keys.length; startIdx += MaxOpsPerCall) {
				await self.#datastore.delete(keys.slice(startIdx, startIdx + MaxOpsPerCall));
			}
			result = { success: true, msg: `Cleared ${keys.length} activities from ${templateName}.` };
		} catch(err) {
			const	errmsg = `Error clearing Datastore(${templateName}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}

		return result;
	}

	async #addActivities(params, template)
	{
		const	self = this;

		if (!params.data || typeof params.data !== 'object')
			return { errmsg: 'Missing data for add.' };

		const	activities = (Array.isArray(params.data) ? params.data : [ params.data ]).filter(act => {
			if (!act.user || typeof(act.user) !== 'string') {
				logger.warn(`Missing user for ${JSON.stringify(act)}`);
				return false;
			}
			if (!act.date || typeof(act.date) !== 'string' || isNaN(new Date(act.date).getTime())) {
				logger.warn(`Missing/invalid date for ${JSON.stringify(act)}`);
				return false;
			}
			if (!Number.isInteger(act.distance)) {
				logger.warn(`Missing/invalid distance for ${JSON.stringify(act)}`);
				return false;
			}
			if (act.submit && !Number.isInteger(act.submit)) {
				logger.warn(`Invalid submit timestamp for ${JSON.stringify(act)}`);
				return false;
			}

			return true;
		});
		if (activities.length === 0)
			return { errmsg: 'No valid data to add.' };

		const	users = {};
		const	keyMap = {};
		for (const act of activities) {
			if (keyMap[act.submit])
				return { errmsg: 'Submit times must be unique.' };

			users[act.user] = 1;
			keyMap[act.submit] = 1;
		}

		let		result = {};
		try
		{
			for (const user of Object.keys(users)) {
				await template.vrUsers.add(user);
			}

			const	items = activities.map(i => template.vrActivities.getDatastoreObject(i));
			let		startIdx;
			for (startIdx = 0; startIdx < items.length; startIdx += MaxOpsPerCall) {
				await self.#datastore.save(items.slice(startIdx, startIdx + MaxOpsPerCall));
			}

			await template.vrResults.administerAll();

			result = { success: true, msg: `Successfully added ${items.length} activities to ${params.template}.` };
		} catch(err) {
			const	errmsg = `Error adding activities to Datastore(${params.template}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}

		return result;
	}

	#matchesFilter(item, params)
	{
		const	self = this;

		if (params.datefrom && (!item.date || item.date < params.datefrom))
			return false;
		if (params.dateto && (!item.date || item.date > params.dateto))
			return false;
		if (params.submitfrom && (!item.submit || item.submit < params.submitfrom))
			return false;
		if (params.submitto && (!item.submit || item.submit > params.submitto))
			return false;
		if (params.user && (!item.user || item.user !== params.user))
			return false;
		if (params.key) {
			const	paramPath = VRAdmin.#getPathFromParams(params);
			const	itemPath = item[Datastore.KEY].path;
			if (paramPath[0] !== itemPath[0] || paramPath[1] !== itemPath[1])
				return false;
		}

		return true;
	}

	async #listItems(params)
	{
		const	self = this;

		if (!params.kind)
			return { errmsg: 'Missing item kind.' };
		if (!AllKinds.includes(params.kind))
			return { errmsg: 'Invalid item kind.' };

		let		result = { items: [] };
		try {
			const query = self.#datastore.createQuery(params.template, params.kind);
			const [items] = await self.#datastore.runQuery(query);

			for (const item of items) {
				if (!self.#matchesFilter(item, params))
					continue;

				result.items.push({ key: item[Datastore.KEY].path, data: item });
			}
			result.success = true;
		} catch(err) {
			const	errmsg = `Error listing Datastore(${params.template}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}	

		return result;
	}

	async #deleteItems(params)
	{
		const	self = this;

		const	path = VRAdmin.#getPathFromParams(params);
		if (path) {
			await self.#datastore.delete(self.#datastore.key({ namespace: params.template, path }));
			return { success: true, msg: 'Deleted.' };
		}

		if (!params.datefrom && !params.dateto && !params.submitfrom && !params.submitto && !params.user && !params.kind)
			return { errmsg: 'No parameters were specified for delete. To remove all activities use "clearall" instead.' };

		let		result = {};
		try {
			const	query = self.#datastore.createQuery(params.template, params.kind);
			const	[items] = await self.#datastore.runQuery(query);
			const	keys = items.filter(item => self.#matchesFilter(item, params)).map(item => item[Datastore.KEY]);

			let	startIdx;
			for (startIdx = 0; startIdx < keys.length; startIdx += MaxOpsPerCall) {
				await self.#datastore.delete(keys.slice(startIdx, startIdx + MaxOpsPerCall));
			}

			result = { success: true, msg: `Deleted ${keys.length} items from ${params.template}/${params.kind}.` };
		} catch(err) {
			const	errmsg = `Error deleting from Datastore(${params.template}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}

		return result;
	}

	async #administerAll(template)
	{
		const	self = this;

		if (!template)
			return { errmsg: 'No template.' };

		let		result = {};
		try {
			await template.vrResults.administerAll();

			result = { success: true, msg: `Successfully administered ${template.name}.` };
		} catch(err) {
			const	errmsg = `Error administering Datastore(${template.name}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}

		return result;
	}

	async #consistencyCheck(kind, template)
	{
		let		result = '';

		if (kind === Kinds.Challenge) {
			await template.vrActivities.reInit();
			result += '\nRe-initialised activities (challenges).';
		}

		if (kind === Kinds.User) {
			await template.vrUsers.init();
			result += '\nRe-initialised users.';
		}
		if (kind === Kinds.Target) {
			await template.vrTargets.init();
			result += '\nRe-initialised targets.';
		}
		// Through changed activities, the results may have been changed.
		if (kind === Kinds.Activity || kind === Kinds.Target) {
			await template.vrResults.administerAll();
			result += '\nRe-administered results.';
		}

		return result;
	}

	async #setItem(params, template)
	{
		const	self = this;

		const	path = VRAdmin.#getPathFromParams(params);
		if (!path || !params.data)
			return { errmsg: 'No parameters were specified for upsert.' };
		if (!template)
			return { errmsg: 'No template.' };

		let		result = {};
		let		result2 = '';
		try {
			const	raw = {
				key: self.#datastore.key({ namespace: params.template, path }),
				data: params.data
			};
			await self.#datastore.save(raw);

			result2 = await self.#consistencyCheck(path[0], template);

			result = { success: true, msg: `Successfully added/modified ${JSON.stringify(path)} in ${params.template}.` + result2 };
		} catch(err) {
			const	errmsg = `Error adding/modifying ${path} in Datastore(${params.template}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}

		return result;
	}

	async #delProp(params, template)
	{
		const	self = this;

		if (!params.kind || !params.property)
			return { errmsg: 'Missing item kind or property.' };
		if (!AllKinds.includes(params.kind))
			return { errmsg: 'Invalid item kind.' };
		if (!template)
			return { errmsg: 'No template.' };

		let		result = {};
		try {
			const query = self.#datastore.createQuery(params.template, params.kind);
			const [items] = await self.#datastore.runQuery(query);

			let		keys = [];
			for (let item of items) {
				if (!self.#matchesFilter(item, params))
					continue;
				if (!item.hasOwnProperty(params.property))
					continue;

				delete item[params.property];

				await self.#datastore.save(item);

				keys.push(item[Datastore.KEY].path);
			}

			result = { success: true, msg: `Successfully deleted ${params.kind}.${params.property} in ${params.template}.` };
			if (items.length > 0) {
				const	result2 = await self.#consistencyCheck(params.kind, template);
				result.msg += result2;
				result.keys = keys;
			}
		} catch(err) {
			const	errmsg = `Error deleting ${params.kind}.${params.property} in Datastore(${params.template}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}

		return result;
	}


	async #setProp(params, template)
	{
		const	self = this;

		if (!params.kind || !params.property || !params.data)
			return { errmsg: 'Missing item kind or property.' };
		if (!AllKinds.includes(params.kind))
			return { errmsg: 'Invalid item kind.' };
		if (!template)
			return { errmsg: 'No template.' };

		let		result = {};
		try {
			const query = self.#datastore.createQuery(params.template, params.kind);
			const [items] = await self.#datastore.runQuery(query);

			let		keys = [];
			let		foundEqual = 0;
			for (let item of items) {
				if (!self.#matchesFilter(item, params))
					continue;
				if (item.hasOwnProperty(params.property) && JSON.stringify(item[params.property]) === JSON.stringify(params.data)) {
					++foundEqual;
					continue;
				}

				item[params.property] = params.data;

				await self.#datastore.save(item);

				keys.push(item[Datastore.KEY].path);
			}
			result.success = true;
			if (keys.length === 0) {
				if (foundEqual > 0)
					result.msg = `Found ${foundEqual} matching, unchanged.`;
				else
					result.msg = '(Not found!)';
			} else {
				const	result2 = await self.#consistencyCheck(params.kind, template);
				result.msg = `Successfully modified ${params.kind}.${params.property} in ${params.template}.` + result2;
				result.keys = keys;
			}
		} catch(err) {
			const	errmsg = `Error modifying ${params.kind}.${params.property} in Datastore(${params.template}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}

		return result;
	}

	async #getStoreItems(templateName, kind)
	{
		const	self = this;
	
		const query = self.#datastore.createQuery(templateName, kind);
		const [items] = await self.#datastore.runQuery(query);

		return items.map(i => { return { key: i[Datastore.KEY].name, data: i }; });
	}

	async #export(templateName)
	{
		const	self = this;

		if (!templateName || templateName.length === 0)
			return { errmsg: 'Error: Missing template name.' };
		
		let		result = {};
		try {
			for (const kind of AllKinds) {
				const	items = await self.#getStoreItems(templateName, kind);
				result[kind] = items;
			}
			result.success = true;
		} catch(err) {
			const	errmsg = `Error exporting Datastore(${templateName}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}	

		return result;
	}

	async #import(params)
	{
		const	self = this;

		if (!params.template || params.template.length === 0)
			return { errmsg: 'Error: Missing template name.' };
		if (!params.data || typeof params.data !== 'object' || Object.keys(params.data).length === 0)
			return { errmsg: 'Error: Missing data.' };

		if (params.clear) {
			for (const kind of AllKinds) {
				try {
					const	query = self.#datastore.createQuery(params.template, kind);
					const	[items] = await self.#datastore.runQuery(query);
					const	keys = items.map(item => item[Datastore.KEY]);
		
					let	startIdx;
					for (startIdx = 0; startIdx < keys.length; startIdx += MaxOpsPerCall) {
						await self.#datastore.delete(keys.slice(startIdx, startIdx + MaxOpsPerCall));
					}
					logger.info(`Cleared ${kind}: ${keys.length} items from ${params.template}.`);
				} catch(err) {
					const	errmsg = `Error clearing Datastore(${params.template}): `;
					logger.error(errmsg, err);
					return { errmsg: errmsg + err };
				}
			}
		}

		let		result = {};
		try {
			let		items = [];
			for (const [key, value] of Object.entries(params.data)) {
				if (!AllKinds.includes(key))
					continue;
				if (!value || !Array.isArray(value))
					continue;

				for (const row of value) {
					if (!row.key || typeof row.key !== 'string')
						continue;

					items.push({
						key: self.#datastore.key({
							namespace: params.template,
							path: [key, row.key]
						}),
						data: row.data
					});
				}
			}

			let	startIdx;
			for (startIdx = 0; startIdx < items.length; startIdx += MaxOpsPerCall) {
				await self.#datastore.save(items.slice(startIdx, startIdx + MaxOpsPerCall));
			}
			result = { success: true, msg: `Imported ${items.length} items to ${params.template}.` };

			logger.warn('Don\'t forget to restart the server to use the data you have just imported. Then call /administer if activities, challenges or targets were changed.');
		} catch(err) {
			const	errmsg = `Error importing to Datastore(${params.template}): `;
			logger.error(errmsg, err);
			return { errmsg: errmsg + err };
		}

		return result;
	}

	async processCommand(template, params)
	{
		const	self = this;

		let		result;
		if (params.command === 'clearall') {
			result = await self.#clearAllActivities(params.template);
		} else if (params.command === 'addactivities') {
			result = await self.#addActivities(params, template);
		} else if (params.command === 'list') {
			result = await self.#listItems(params);
		} else if (params.command === 'delete') {
			result = await self.#deleteItems(params);
		} else if (params.command === 'set') {
			result = await self.#setItem(params, template);
		} else if (params.command === 'delprop') {
			result = await self.#delProp(params, template);
		} else if (params.command === 'setprop') {
			result = await self.#setProp(params, template);
		} else if (params.command === 'administer') {
			result = await self.#administerAll(template);
		} else if (params.command === 'export') {
			result = await self.#export(params.template);
		} else if (params.command === 'import') {
			result = await self.#import(params);
		}

		return result;
	}
}


module.exports = VRAdmin;
