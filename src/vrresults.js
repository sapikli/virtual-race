/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const logger = require('./logger');
const VRActivities = require('./vractivities');
const VRTargets = require('./vrtargets');
const VRUsers = require('./vrusers');


// Virtual race section/finish result administration
class VRResults {
	#vrActivities;
	#vrTargets;
	#vrUsers;
	#achievements;
	#monthly;

	constructor(vrActivities, vrTargets, vrUsers)
	{
		const	self = this;

		self.#vrActivities = vrActivities;
		self.#vrTargets = vrTargets;
		self.#vrUsers = vrUsers;
		self.#achievements = [];
		self.#monthly = {};
	}

	#giveAchievements(activities, user)
	{
		const	self = this;

		let		mine = activities.filter(item => item.user === user).sort((a, b) => {
			if (a.date !== b.date)
				return (a.date < b.date ? -1 : 1);
			return a.submit - b.submit;
		});

		const	targets = self.#vrTargets.getTargets();
		let		totalDist = 0;
		let		i = 0;
		for (const act of mine) {
			totalDist += act.distance;
			for ( ; i < targets.length && totalDist >= targets[i].distance; ++i) {
				self.#achievements.push({
					user,
					target: targets[i].distance,
					date: act.date,
					submit: act.submit
				});
			}
			if (i >= targets.length)
				break;
		}
	}

	#calculateMonthly(activities, user)
	{
		const	self = this;

		let		monthly = {};
		for (const act of activities) {
			const	pos = act.date.lastIndexOf('-');
			if (act.user !== user || pos < 0)
				continue;

			const	key = act.date.slice(0, pos);
			monthly[key] = act.distance + (monthly[key] ?? 0);
		}

		const	thisMonth = new Date().toISOString().slice(0, 7);
		for (const key of Object.keys(monthly)) {
			if (key >= thisMonth)
				continue;
			if (!self.#monthly.hasOwnProperty(key))
				self.#monthly[key] = [];
			self.#monthly[key].push({ user, distance: monthly[key]});
		}
	}

	async #allAchievements()
	{
		const	self = this;

		self.#achievements = [];
		self.#monthly = {};

		let		users = Object.keys(self.#vrUsers.getAll());

		const	activities = await self.#vrActivities.get();
		for (const act of activities) {
			users.push(act.user);
		}

		const	uniqueUsers = users.filter((value, index, self) => self.indexOf(value) === index);
		for (const user of uniqueUsers) {
			self.#giveAchievements(activities, user);
			self.#calculateMonthly(activities, user);
		}
	}

	async administer(user)
	{
		const	self = this;

		if (!user || !self.#vrTargets.hasTargets())
			return;

		const	activities = await self.#vrActivities.get();

		self.#achievements = self.#achievements.filter(item => item.user !== user);
		for (let month of Object.keys(self.#monthly)) {
			self.#monthly[month] = self.#monthly[month].filter(item => item.user !== user);
		}
		self.#giveAchievements(activities, user);
		self.#calculateMonthly(activities, user);
	}

	async administerAll()
	{
		const	self = this;

		if (!self.#vrTargets.hasTargets())
			return;

		await self.#allAchievements();
	}

	async init()
	{
		const	self = this;

		await self.administerAll();
	}

	// Get finishers of each target, sorted by date
	getFinishers()
	{
		const	self = this;

		const	targets = self.#vrTargets.getTargets();

		let		result = {};
		for (const target of targets) {
			result[target.distance] = self.#achievements
				.filter(item => item.target === target.distance)
				.sort((a, b) => {
					if (a.date !== b.date)
						return (a.date < b.date ? -1 : 1);
					return a.submit - b.submit;
				})
				.map(item => item.user);
		}

		return result;
	}

	getBadges(allFinishers, user)
	{
		const	self = this;

		if (!user || user.length === 0)
			return null;

		let		result = { badges: [] };
		const	targets = self.#vrTargets.getTargets();
		const	keys = Object.keys(allFinishers).sort((a, b) => parseInt(a) - parseInt(b));
		for (const key of keys) {
			const	finishers = allFinishers[key];
			const	place = finishers.indexOf(user);
			if (place < 0)
				continue;

			const	distance = parseInt(key);
			let		target;
			for (const item of targets) {
				if (item.distance === distance) {
					target = item;
					break;
				}
			}
			if (!target) {
				logger.warn(`Target not found for ${distance} / ${user}.`);
				continue;
			}
			if (target.finish) {
				result.place = place + 1;
				for (const ach of self.#achievements) {
					if (ach.user === user && ach.target === distance) {
						result.finishdate = ach.date;
						break;
					}
				}
			}

			let		badge = Object.assign({}, target);
			if (!badge.nomedal && place < 3)
				badge.place = place + 1;

			result.badges.push(badge);
		}

		return result;
	}

	getMonthly()
	{
		const	self = this;

		const	userMedals = [];
		const	users = new Set();
		for (const month of Object.keys(self.#monthly)) {
			if (self.#monthly[month].length < 1)
				continue;

			self.#monthly[month].sort((a, b) => b.distance - a.distance);

			const	prev = self.#monthly[month][0].distance;
			let		place = 1;
			userMedals.push({ user: self.#monthly[month][0].user, month, place, distance: self.#monthly[month][0].distance });
			users.add(self.#monthly[month][0].user);

			let		i;
			for (i = 1; i < self.#monthly[month].length; ++i) {
				if (self.#monthly[month][i].distance !== prev) {
					++place;
					if (place > 3)
						break;
					if (i > 3)
						break;
				}
				userMedals.push({ user: self.#monthly[month][i].user, month, place, distance: self.#monthly[month][i].distance });
				users.add(self.#monthly[month][i].user);
			}
		}

		userMedals.sort((a, b) => {
			if (a.month < b.month)
				return -1;
			if (a.month > b.month)
				return 1;
			return a.place - b.place;
		});

		let		monthlyResult = [];
		for (const user of users.keys()) {
			let		a1 = [];
			let		a2 = [];
			let		a3 = [];
			for (const medal of userMedals) {
				if (medal.user !== user)
					continue;

				switch (medal.place) {
					case 1:		a1.push({ month: medal.month, distance: medal.distance });	break;
					case 2:		a2.push({ month: medal.month, distance: medal.distance });	break;
					case 3:		a3.push({ month: medal.month, distance: medal.distance });	break;
				}
				if (a1.length + a2.length + a3.length === 0)
					continue;
			}


			const	mo = {};
			for (const month of Object.keys(self.#monthly).sort()) {
				let		md = 0;
				for (const m of self.#monthly[month]) {
					if (m.user === user) {
						md = m.distance;
						break;
					}
				}

				let		mp = 0;
				for (const m of userMedals) {
					if (m.user === user && m.month === month) {
						mp = m.place;
						break;
					}
				}

				mo[month] = { distance: md, place: mp };
			}
	
			monthlyResult.push({
				user,
				m1: a1,
				m2: a2,
				m3: a3,
				months: mo
			});
		}

		return monthlyResult;
	}
}


module.exports = VRResults;
