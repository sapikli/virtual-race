/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const logger = require('./logger');


// WGS-84 ellipsoid parameters
const RLONG_EARTH = 6378137.0;
const RLAT_EARTH = 6356752.3142451795;


// A very rudimentary approximation instead of proper geodesic calculation
function getWGS84Distance(coord1, coord2)
{
	const	dCosY = Math.cos((coord1[1] + coord2[1]) * Math.PI / 360);
	const	yD = RLAT_EARTH * (coord2[1] - coord1[1]);
	const	xD = RLONG_EARTH * (coord2[0] - coord1[0]) * dCosY;

	return 1.00185 * Math.sqrt(xD * xD + yD * yD) * Math.PI / 180;		// Adjust for the meridional radius of curvature, in Central Europe.
}


function fromWGS84ToGM(lat,lon)		// Google Mercator, EPSG:3857
{
	let	result = {};
	const	dSinFi = Math.sin(lat * Math.PI / 180.0);
	if (Math.abs(dSinFi - 1.0) > 1.0e-12) {
		result.x = (RLONG_EARTH * lon * Math.PI / 180.0);
		// Google projects as if the ellipsoid was a sphere.
		result.y = (RLONG_EARTH / 2.0) * Math.log((1.0 + dSinFi) / (1.0 - dSinFi));
	}

	return result;
}


// Calculate track position
class TrackInterpolator {
	#coords;
	#overflow;
	#flowback;
	#trackLen;
	#cumulativeLen;
	#trackBounds;

	constructor(coords, overflow, flowback)
	{
		const	self = this;

		self.#coords = coords.filter(item => Array.isArray(item) && (item.length >= 2));
		self.#overflow = overflow;
		self.#flowback = flowback;
		self.#trackLen = 0;

		if (coords && coords.length >= 2) {
			const	cnt = coords.length;
			let		i;
			let		len = 0;
			self.#cumulativeLen = [ 0 ];
			let		minLat = coords[0][1];
			let		maxLat = coords[0][1];
			let		minLon = coords[0][0];
			let		maxLon = coords[0][0];
			for (i = 1; i < cnt; ++i) {
				const	sectionLen = getWGS84Distance(coords[i - 1], coords[i]);
				if (sectionLen < 1.0e-3) {
					logger.warn(`Duplicate point at ${i} = [${coords[i][0]},${coords[i][1]}].`);
					self.#coords[i] = [];
					continue;
				}

				len += sectionLen;
				self.#cumulativeLen.push(len);

				if (minLat > coords[i][1])
					minLat = coords[i][1];
				if (maxLat < coords[i][1])
					maxLat = coords[i][1];
				if (minLon > coords[i][0])
					minLon = coords[i][0];
				if (maxLon < coords[i][0])
					maxLon = coords[i][0];
			}
			self.#trackLen = len;
			self.#coords = self.#coords.filter(item => item.length >= 2);
			self.#trackBounds = [ [ minLat, minLon ], [ maxLat, maxLon ] ];
		}

/*		
		const	pt = [ 1.234, 5.678 ];		// lat, lon
		const	d = self.#getDistFromCoord(pt[0], pt[1]);
		console.log(`${d} -> ${2 * self.#trackLen - d}`);
		let		pd = self.getCoordFromDist(d);
		console.log(pd);
		console.log(getWGS84Distance(pt, pd));

		pd = self.getCoordFromDist(d - 3);
		console.log(pd);
		console.log(getWGS84Distance(pt, pd));

		pd = self.getCoordFromDist(d + 3);
		console.log(pd);
		console.log(getWGS84Distance(pt, pd));
*/		
	}

	getStartCoords()
	{
		const	self = this;
		if (!self.#coords || self.#coords.length < 2)
			return null;

		return [self.#coords[0][1], self.#coords[0][0]]
	}

	getFinishCoords()
	{
		const	self = this;
		if (!self.#coords || self.#coords.length < 2)
			return null;

		const	last = self.#coords.at(-1);

		return [last[1], last[0]];
	}

	getBounds()
	{
		const	self = this;
		if (!self.#coords || self.#coords.length < 2)
			return null;

		return self.#trackBounds;
	}

	getLength()
	{
		const	self = this;
		return self.#trackLen;
	}

	getCoordFromDist(dist)
	{
		const	self = this;

		if (!self.#coords || self.#coords.length < 2 || self.#trackLen === 0)
			return null;
		if ((typeof dist !== 'number') || isNaN(dist) || dist <= 0)
			return self.getStartCoords();
		if (dist >= self.#trackLen) {
			if (!self.#overflow)
				return self.getFinishCoords();

			const	lengths = Math.floor(dist / self.#trackLen);
			dist %= self.#trackLen;

			const	last = self.#coords.at(-1);
			if (self.#flowback) {
				// Open route: bounce back from finish (and from start)
				if ((lengths % 2) == 0) {
					// Already completed a return trip.
				} else {
					// Coming back
					dist = self.#trackLen - dist;
				}
			} else {
				// Start another round
			}
		}

		const	cnt = self.#coords.length;
		let		lower = 0;
		let		upper = cnt;
		let		i = 0;
		for ( ; ; ) {
			i = Math.floor((lower + upper) / 2);
			if (dist < self.#cumulativeLen[i]) {
				if (i === upper)
					break;
				upper = i;
			} else {
				if (i === lower)
					break;
				lower = i;
			}
		}

		const	ratio = (dist - self.#cumulativeLen[i]) / (self.#cumulativeLen[i + 1] - self.#cumulativeLen[i]);
		const	dx = self.#coords[i + 1][0] - self.#coords[i][0];
		const	dy = self.#coords[i + 1][1] - self.#coords[i][1];
	
		return [ self.#coords[i][1] + dy * ratio, self.#coords[i][0] + dx * ratio ];
	}

	#getDistFromCoord(lat, lon)
	{
		const	self = this;
		if (!self.#coords || self.#coords.length < 2)
			return 0;

		const	pt = [ lon, lat ];
		let		bestIdx = 0;
		let		minDist = getWGS84Distance(pt, self.#coords[0]);
		let		i;
		for (i = 1; i < self.#coords.length; ++i) {
			const	dist = getWGS84Distance(pt, self.#coords[i]);
			if (dist < minDist) {
				bestIdx = i;
				minDist = dist;
			}
		}

		let		p1, d1, l1;
		const	distprev = getWGS84Distance(pt, self.#coords[bestIdx - 1]);
		const	distnext = getWGS84Distance(pt, self.#coords[bestIdx + 1]);
		if (distprev < distnext) {
			p1 = self.#coords[bestIdx - 1];
			d1 = distprev;
			l1 = self.#cumulativeLen[bestIdx - 1] - self.#cumulativeLen[bestIdx];
		} else {
			p1 = self.#coords[bestIdx + 1];
			d1 = distnext;
			l1 = self.#cumulativeLen[bestIdx + 1] - self.#cumulativeLen[bestIdx];
		}

		return self.#cumulativeLen[bestIdx] + l1 * minDist / (minDist + d1);
	}

	getGMTrack()
	{
		const	self = this;

		return {
			bounds: self.#trackBounds.map(pt => fromWGS84ToGM(pt[0], pt[1])),	// self.#trackBounds are in lat-lon
			track: self.#coords.map(pt => fromWGS84ToGM(pt[1], pt[0]))			// self.#coords are in lon-lat
		};
	}
}


module.exports = {
	fromWGS84ToGM: fromWGS84ToGM,
	TrackInterpolator: TrackInterpolator
}
