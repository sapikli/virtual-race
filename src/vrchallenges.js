/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const logger = require('./logger');
const { Datastore } = require('@google-cloud/datastore');
const { Kinds } = require('./kinds');


// Virtual race challenges (distance in one activity)
class VRChallenges {
	#datastore;
	#template;
	#challenges;

	constructor(datastore, template)
	{
		const	self = this;

		self.#datastore = datastore;
		self.#template = template;
		self.#challenges = [];
	}

	async init(force)
	{
		const	self = this;

		if (self.#challenges.length > 0 && !force)
			return;

		self.#challenges = [];
		try {
			const query = self.#datastore.createQuery(self.#template, Kinds.Challenge);
			const [items] = await self.#datastore.runQuery(query);

			self.#challenges = items;
			self.#challenges.forEach(item => {
				item.distance = parseInt(item[Datastore.KEY].name);
			});

			self.#challenges = self.#challenges
				.filter(i => Number.isInteger(i.distance) && i.icon && i.icon.length > 0 && i.name && i.name.length > 0)
				.sort((a, b) => (b.distance - a.distance));
		} catch(err) {
			logger.error('Error querying the Datastore: ', err);
		}
	}

	get(distance)
	{
		const	self = this;

		for (const challenge of self.#challenges) {
			if (distance >= challenge.distance)
				return { distance: challenge.distance, icon: challenge.icon, name: challenge.name };
		}

		return null;
	}
}


module.exports = VRChallenges;
