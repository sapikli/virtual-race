/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const logger = require('./logger');
const crc = require('crc');
const { Datastore } = require('@google-cloud/datastore');
const { Kinds } = require('./kinds');


// Virtual race users functionality
class VRUsers {
	#datastore;
	#template;
	#users;

	constructor(datastore, templateName)
	{
		const	self = this;

		self.#datastore = datastore;
		self.#template = templateName;
		self.#users = {};
	}

	async init()
	{
		const	self = this;

		self.#users = {};
		try {
			const query = self.#datastore.createQuery(self.#template, Kinds.User);
			const [users] = await self.#datastore.runQuery(query);

			for (const item of users) {
				self.#users[item[Datastore.KEY].name] = item;
			}
		} catch(err) {
			logger.error('Error querying the Datastore: ', err);
		}
	}

	exists(user)
	{
		const	self = this;

		return self.#users.hasOwnProperty(user);
	}

	getByAlias(alias)
	{
		const	self = this;

		for (const k of Object.keys(self.#users)) {
			if (self.#users[k].hasOwnProperty('alias') && self.#users[k].alias === alias)
				return Object.assign({ name: k }, self.#users[k]);
		}

		return null;
	}

	get(user)
	{
		const	self = this;

		const	userCrc = crc.crc32(Buffer.from(user ?? '', 'utf8')) & 0xFFFFFF;
		return Object.assign({
			// Create default colours
			bgcolor: '#' + userCrc.toString(16).padStart(6, '0'),
			fgcolor: '#' + (userCrc ^ 0xFFFFFF).toString(16).padStart(6, '0')
		}, self.#users[user]);
	}

	getAll()
	{
		const	self = this;

		return self.#users;
	}

	// Add if not exists
	async add(user)
	{
		const	self = this;

		if (self.#users.hasOwnProperty(user))
			return;

		// Create with defaults
		await self.set(user, self.get(user));
	}

	async set(user, data)
	{
		const	self = this;
		let		result = {
			success: false
		};

		self.#users[user] = data;

		const	rec = {
			key: self.#datastore.key({
				namespace: self.#template,
				path: [Kinds.User, user]
			}),
			data
		};

		try {
			await	self.#datastore.save(rec);
			result.success = true;
		}
		catch(err) {
			logger.error('Failed to set: ', err);
			result.errormsg = 'Failed to set.';
		}

		return result;
	}

	async delete(user)
	{
		const	self = this;
		let		result = {
			success: false,
			errormsg: 'No such user.'
		};

		if (self.#users[user]) {
			delete self.#users[user];

			try {
				await self.#datastore.delete(self.#datastore.key({
					namespace: self.#template,
					path: [Kinds.User, user]
				}));
				result.success = true;
				delete result.errormsg;
			} catch(err) {
				logger.error(`Error deleting user: `, err);
				result.errormsg = 'Error deleting.';
			}
		}

		return result;
	}
}


module.exports = VRUsers;
