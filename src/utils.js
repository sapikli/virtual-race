/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

function getPkgJsonDir ()
{
	const { existsSync } = require('fs');
	const { dirname } = require('path');

	for (const mpath of module.paths) {
		if (existsSync(mpath))
			return dirname(mpath);
	}
}


module.exports = {
	getPkgJsonDir: getPkgJsonDir
}
