/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/


// Create environment variables listed in .env file
if (process.env.NODE_ENV !== 'production')
	require('dotenv').config();

// Init logger
const logger = require('./logger');

logger.info('');
logger.info('*                                                 *');
logger.info('***************************************************');


const async = require('async');
const compression = require('compression');
const express = require('express');
const bodyParser = require('body-parser');
const bodyParserErrorHandler = require('express-body-parser-error-handler');
const cors = require('cors');
const fs = require('fs');
const os = require('os');
const path = require('path');
const { Datastore } = require('@google-cloud/datastore');
const { createProxyMiddleware } = require('http-proxy-middleware');

const { Kinds } = require('./kinds');
const { TrackInterpolator } = require('./trackinterpolator');
const { getPkgJsonDir } = require('./utils');
const VRAdmin = require('./vradmin');
const VRActivities = require('./vractivities');
const VRLists = require('./vrlists');
const VRPaper = require('./vrpaper');
const VRTargets = require('./vrtargets');
const VRUsers = require('./vrusers');
const VRResults = require('./vrresults');
let	server;


let		dsParams;
if (process.env.DS_ENDPOINT && process.env.DS_ENDPOINT.length > 0) {
	try {
		dsParams = JSON.parse(process.env.DS_ENDPOINT);
	}
	catch(err)
	{
		logger.warn('Error parsing DS_ENDPOINT: ', err);
	}
}

if (dsParams)
	logger.info(`Using Datastore ${JSON.stringify(dsParams)}`);
else
	logger.info('Using project default Datastore.');
const	datastore = new Datastore(dsParams);

const	templateDir = path.join(getPkgJsonDir(), 'vrtemplates');
let		templates = [];
let		auths = [];
const	pkg = require(path.join(getPkgJsonDir(), 'package.json'));


// App
const app = express();

app.use(cors());
app.options('*', cors());
try {
//	const	baseurl = fs.readFileSync(path.join(getPkgJsonDir(), 'secret/baseurl')).toString().trim();
//	app.options(baseurl, cors());
}
catch(_err)
{
}


try {
	const	redirs = JSON.parse(fs.readFileSync(path.join(getPkgJsonDir(), 'secret/redirects.json')).toString());
	for (const r of redirs) {
		if (typeof r !== 'object') {
			logger.error(`Invalid entry in redirects.json. Expecting an array of objects.`);
			continue;
		}
		if (!r.hasOwnProperty('source') || typeof r.source !== 'string' || !r.source.startsWith('/')
			|| !r.hasOwnProperty('target') || typeof r.target !== 'string' || !r.target.startsWith('http')) {
			logger.error(`Invalid entry in redirects.json. Either source or target string is missing.`);
			continue;
		}

		app.use(
			'/redirect' + r.source,
			createProxyMiddleware({
				target: r.target,
				changeOrigin: true,
				secure: true,
				logLevel: 'warn',
				followRedirects: true
			})
		);
	}
}
catch(err)
{
	if (err.code !== 'ENOENT')
		logger.error(err.message);
}


app.use((err, req, res, next) => {
	if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
	  // Handle bad request error due to invalid JSON payload
	  res.status(400).json({ error: 'Bad request' });
	} else {
	  // Handle other errors
	  next(err);
	}
});

app.disable('x-powered-by');
app.use(compression());
app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({limit: '1mb', extended: true}));
app.use(bodyParserErrorHandler());

let isReady = 0;	// 0 - db check in progress, <0 - db check failed, >0 - db check succeeded

// Log requests
app.use((req, res, next) => {
	const url = req.url;
	logger.log('http', 'req>>: ' + req.method + ' ' + url);
	res.on('finish', () => {
		logger.log('http', '<<res: ' + req.method + ' ' + url);
	});
	next();
});


if (process.env.NODE_ENV === 'production') {
	app.set('trust proxy', true);
	logger.info('Trust proxy is set to true.');
}


function checkHealthState(readyness, req, res) {
	res.setHeader('Content-Type', 'text/plain');
	if (isReady > 0) { // after successful initialization => check db availability
		res.status(200).send('OK');
	} else if (isReady === 0) { // initialization in progress
		if (readyness) {
			res.status(503).end();
		} else {
			res.status(200).send('OK');
		}
	} else {  // initialization failed
		res.status(500).end();
		app.gracefulExit(0);
	}

}
// Liveness probe (Kubernetes)
app.use('/isalive', (req, res) => checkHealthState(false, req, res));

// Readiness probe (Kubernetes)
app.use('/isready', (req, res) => checkHealthState(true, req, res));

app.use('/version', (req, res) => {
	res.json({
		success: true,
		version: pkg.version,
		module: `${pkg.name} v${pkg.version}`
	});
});


app.use('/admin', async (req, res) => {
	if (req.method !== 'POST' || !req.body.command) {
		res.status(400).send('Bad Request');
		return;
	}
	if (!req.body.auth || !auths.includes(req.body.auth)) {
		res.status(403).send('Forbidden');
		return;
	}

	if (req.body.command === 'listtemplates') {
		const	list = { success: true, templates: templates.map(t => t.name) };
		res.status(200).send(list);
		return;
	}

	let		template;
	if (req.body.template) {
		for (const t of templates) {
			if (req.body.template === t.name) {
				template = t;
				break;
			}
		}
	}

	if (template && req.body.command === 'getresults') {
		await getContent(template, '{}', res);
		return;
	}

	let		result;
	try {
		const	vrAdmin = new VRAdmin(datastore);
		result = await vrAdmin.processCommand(template, req.body);
	}
	catch(err)
	{
		logger.error('Admin error: ', err);
		res.status(500).send('Internal Server Error');
		return;
	}

	if (typeof result !== 'undefined') {
		if (typeof result === 'object') {
			res.json(result);
		} else {
			if (result.startsWith('{'))
				res.setHeader('Content-Type', 'application/json');
			else
				res.setHeader('Content-Type', 'text/plain');
			res.status(200).send(result);
		}
	} else {
		res.status(400).send('Bad Request');
	}
});


app.use('/restart', async (req, res) => {
	if (req.method !== 'POST' || !req.body) {
		res.status(400).send('Bad Request');
		return;
	}
	if (!req.body.auth || !auths.includes(req.body.auth)) {
		res.status(403).send('Forbidden');
		return;
	}

	try {
		for (let template of templates) {
			logger.info(`Initialising ${template.name}`);
			await template.vrActivities.reInit();
			await template.vrTargets.init();
			await template.vrUsers.init();
			await template.vrResults.init();
		}
	}
	catch(err)
	{
		logger.error('Admin error: ', err);
		res.status(500).send('Internal Server Error');
		return;
	}

	res.setHeader('Content-Type', 'text/plain');
	res.status(200).send('Successfully restarted server.');
});


function getAllFiles(dirPath, arrayOfFiles)
{
	const files = fs.readdirSync(dirPath);
  
	arrayOfFiles = arrayOfFiles || [];
  
	files.forEach(file => {
		if (fs.statSync(dirPath + "/" + file).isDirectory() && file !== 'node_modules') {
			arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
		} else {
			arrayOfFiles.push(path.join(dirPath, "/", file));
		}
	});
  
	return arrayOfFiles;
}


async function addActivity(template, body, res)
{
	try {
		if (body.alias) {
			const	user = template.vrUsers.getByAlias(body.alias.trim());
			if (user)
				body.name = user.name;
		}

		if (body.name)
			body.name = body.name.replace(new RegExp('{', 'g'), '[').replace(new RegExp('}', 'g'), ']').trim();

		if (!body.name || body.name.length === 0) {
			res.setHeader('Content-Type', 'application/json');
			res.status(200).send(JSON.stringify({ result: false, errormsg: 'Missing name.' }));
			return;
		}

		if ((body.membersonly || template.config.membersonly) && !template.vrUsers.exists(body.name)) {
			res.status(200).send(JSON.stringify({ result: false, nomember: true }));
			return;
		}

		if (body.date) {
			if (body.date < template.config.startdate) {
				res.status(200).send(JSON.stringify({ result: false, tooearly: true }));
				return;
				}
			if (body.date > template.config.enddate) {
				res.status(200).send(JSON.stringify({ result: false, toolate: true }));
				return;
			}
		}

		await template.vrUsers.add(body.name);

		let		addResult;
		if (!body.repeated) {
			const	last = await template.vrActivities.getLast(body.name);
			if (last && last.distance === body.distance && last.date === body.date) {
				addResult = {
					success: false,
					doublewarning: true
				};
			}
		}

		if (!addResult) {
			addResult = await template.vrActivities.add(body);
			if (addResult.success)
				await template.vrResults.administer(body.name);
		}

		res.setHeader('Content-Type', 'application/json');
		res.status(200).send(JSON.stringify(addResult));
	}
	catch(err)
	{
		logger.error('Error handling request: ', err);
		res.status(500).send('Internal error');
	}
}


async function getContent(template, user, res)
{
	if (!template.interpolator || !template.vrResults || !template.vrTargets || !template.vrUsers) {
		logger.error('Invalid internal state.');
		res.status(500).send('Internal error');
		return;
	}

	try {
		const	activities = await template.vrActivities.get();
		const	realUser = (user === '{}' ? '' : user);

		let		content = VRLists.createContent(activities, VRActivities.filterDummies(activities), realUser);
		content.footer = `${pkg.name} v${pkg.version} @ ${new Date().toISOString()}`;
		content.config = template.config;

		content.startcoords = template.interpolator.getStartCoords();
		const	finishcoords = template.interpolator.getFinishCoords();
		if (finishcoords && (content.startcoords[0] !== finishcoords[0] || content.startcoords[1] !== finishcoords[1]))
			content.finishcoords = finishcoords;
		content.trackbounds = template.interpolator.getBounds();
		content.tracklength = template.interpolator.getLength();

		if (content.list) {
			for (let item of content.list) {
				item.pos = template.interpolator.getCoordFromDist(item.distance);
			}
		}
		if (content.mine) {
			for (let item of content.mine) {
				if (item.total < template.interpolator.getLength())
					item.pos = template.interpolator.getCoordFromDist(item.total);
			}
		}

		content.maptargets = template.vrTargets.getTargets().filter(item => item.showonmap);
		for (let item of content.maptargets) {
			item.pos = template.interpolator.getCoordFromDist(item.distance);
		}

		const	allFinishers = template.vrResults.getFinishers();
		let		allMonthly;
		if (template.config.showmonthly)
			allMonthly = template.vrResults.getMonthly();

		let		tows = [ 0, 0 ];
		if (content.list && content.list.length > 0) {
			let		i;
			for (i = 0; i < content.list.length; ++i) {
				const	props = template.vrUsers.get(content.list[i].user);
				let		userBadges;
				let		monthly;
				if (allFinishers) {
					userBadges = template.vrResults.getBadges(allFinishers, content.list[i].user);
					userBadges.badges = userBadges.badges.filter(item => item.place && !item.hideonlist);
				}
				if (allMonthly) {
					for (const m of allMonthly) {
						if (m.user === content.list[i].user) {
							monthly = m;
							delete monthly.user;
							break;
						}
					}
				}

				content.list[i] = Object.assign({}, props, userBadges, { monthly }, content.list[i]);

				if (props.team && props.team >= 1 && props.team <= 2)
					tows[props.team - 1] += content.list[i].distance;
			}
			content.list.sort((a, b) => {
				if (a.place && !b.place)
					return -1;
				if (!a.place && b.place)
					return 1;
				if (a.place && b.place)
					return a.place - b.place;
				if (a.distance !== b.distance)
					return b.distance - a.distance;
				return a.count - b.count;
			});
		}
		if (tows[0] !== 0 && tows[1] !== 0)
			content.tow = tows;

		let	myResult;
		if (allFinishers)
			myResult = template.vrResults.getBadges(allFinishers, realUser);

		if (content.mine && content.mine.length > 0 && myResult) {
			let		i = 0;
			for (let act of content.mine) {
				for ( ; i < myResult.badges.length && act.total >= myResult.badges[i].distance; ++i) {
					if (!myResult.badges[i].hideonmine)
						act.milestone = myResult.badges[i];
				}
			}
			content.mine.reverse();
			delete content.mine[0].pos;		// We already have the user marker at the current position.
		}
		content.myresult = myResult;

		if (template.config.jointdistance) {
			content.jointdone = 0;
			for (const i of content.list) {
				content.jointdone += i.distance;
			}

			content.jointdays = {};
			for (const a of activities) {
				if (content.jointdays[a.date])
					content.jointdays[a.date] += a.distance;
				else
					content.jointdays[a.date] = a.distance;
			}
		}

		const	ds = new Date().toISOString();
		if (user !== '{}' && template.config.blackoutfrom && ds > template.config.blackoutfrom) {
			if (!template.config.blackoutuntil || ds < template.config.blackoutuntil) {
				content.blackout = true;
				content.latest = [];
				content.list = [];
			}
		}

		res.setHeader('Content-Type', 'application/json');
		res.status(200).send(JSON.stringify(content));
	}
	catch(err)
	{
		logger.error('Error handling request: ', err);
		res.status(500).send('Internal error');
	}
}

async function getPrint(template, user, res)
{
	try {
		if (!user || user.length === 0) {
			res.status(400).send('Bad Request');
			return;
		}
		if (!template.gmTrack || !template.gmTrack.hasOwnProperty('bounds') || !template.gmTrack.hasOwnProperty('track')) {
			logger.error('Invalid state.');
			res.status(500).send('Internal error');
			return;
		}

		if (!template.vrPaper || !template.vrPaper.isValid()) {
			res.status(503).end();
			return;
		}

		const	allFinishers = template.vrResults.getFinishers();
		if (!allFinishers) {
			logger.error('Invalid state.');
			res.status(500).send('Internal error');
			return;
		}

		let		allMonthly;
		let		myMonths;
		if (template.config.showmonthly) {
			allMonthly = template.vrResults.getMonthly();
			if (allMonthly) {
				for (const m of allMonthly) {
					if (m.user === user) {
						myMonths = m.months;
						break;
					}
				}
			 }
		}

		const	activities = await template.vrActivities.get();
		let		content = VRLists.createContent(activities, VRActivities.filterDummies(activities), user);
		if (!content.mine || content.mine.length === 0) {
			res.status(400).send('Bad Request');
			return;
		}

		let		maxcount = 0;
		for (const item of content.list) {
			if (maxcount < item.count)
				maxcount = item.count;
		}

		const	userprops = Object.assign({ name: user }, template.vrUsers.get(user));
		const	medals = template.vrResults.getBadges(allFinishers, user);
		const	ishardest = (content.mine.length >= 10 && content.mine.length === maxcount);

		res.setHeader('Content-Type', 'image/svg+xml; charset=utf-8');
		res.status(200).send(template.vrPaper.get(userprops, medals, template.vrTargets.getTargets(), content.mine, myMonths, content.longest, ishardest));
	}
	catch(err)
	{
		logger.error('Error handling request: ', err);
		res.status(500).send('Internal error');
	}
}

async function registerUser(template, body, res)
{
	try {
		if (!body.name || (typeof body.name !== 'string')) {
			logger.error('Missing name');
			res.status(400).send('Bad Request');
			return;
		}

		body.name = body.name.replace(new RegExp('{', 'g'), '[').replace(new RegExp('}', 'g'), ']').trim();
		if (body.name.length === 0) {
			res.setHeader('Content-Type', 'application/json');
			res.status(200).send(JSON.stringify({ result: false, missing: 'name' }));
			return;
		}
		if (template.vrUsers.exists(body.name)) {
			res.status(200).send(JSON.stringify({ result: false, exists: true }));
			return;
		}

		let		data = {
			registered: new Date().getTime()
		};

		if (body.bgcolor && (typeof body.bgcolor === 'string')) {
			data.bgcolor = body.bgcolor.trim();
			if (data.bgcolor.length !== 7 || data.bgcolor[0] !== '#') {
				res.setHeader('Content-Type', 'application/json');
				res.status(200).send(JSON.stringify({ result: false, missing: 'bgcolor' }));
				return;
			}
		}
		if (body.fgcolor && (typeof body.fgcolor === 'string')) {
			data.fgcolor = body.fgcolor.trim();
			if (data.fgcolor.length !== 7 || data.fgcolor[0] !== '#') {
				res.setHeader('Content-Type', 'application/json');
				res.status(200).send(JSON.stringify({ result: false, missing: 'fgcolor' }));
				return;
			}
		}
		if (body.official && (typeof body.official === 'string')) {
			body.official = body.official.trim();
			if (body.official.length > 0)
				data.official = body.official;
		}
		if (body.svg && (typeof body.svg === 'string')) {
			if (body.svg.startsWith('<svg') && body.svg.endsWith('</svg>'))
				data.svg = body.svg;
			else
				logger.error('Missing/incorrect SVG received');
		}
		if (body.team && Number.isInteger(body.team))
			data.team = body.team;

		const	result = await template.vrUsers.set(body.name, data);

		res.setHeader('Content-Type', 'application/json');
		res.status(200).send(JSON.stringify(result));
	}
	catch(err)
	{
		logger.error('Error handling request: ', err);
		res.status(500).send('Internal error');
	}
}

async function clientRoute(template, req, res)
{
	const	method = req.method.toUpperCase();
	if (req.path === '/add' && method === 'POST') {
		await addActivity(template, req.body, res);
	} else if (req.path === '/get' && method === 'GET') {
		await getContent(template, req.query.user, res);
	} else if (req.path === '/get' && method === 'POST') {
		await getContent(template, req.body.user, res);
	} else if (req.path === '/print' && method === 'GET') {
		await getPrint(template, req.query.user, res);
	} else if (req.path === '/print' && method === 'POST') {
		await getPrint(template, req.body.user, res);
	} else if (req.path === '/register' && method === 'POST') {
		await registerUser(template, req.body, res);
	} else {
		res.status(400).send('Bad Request');
	}
}


// You can stop application from browser, but only in development environment
if (process.env.NODE_ENV !== 'production') {
	app.use('/quit', (req, res) => {
		res.setHeader('Content-Type', 'text/plain');
		res.status(200).send('Bye!');
		app.gracefulExit(0);
	});
}

app.use((req, res, next) => {
	if (isReady < 0) {
		res.status(500).end();
	} else {
		next();
	}
});


///////////////////////////////////////////////////////////////////////////////
// Graceful exit
///////////////////////////////////////////////////////////////////////////////

app.gracefulExit = function gracefulExit(code) {
	async.waterfall([
		(cb) => {
			if (server && server._handle) {
				server.close(() => {
					setImmediate(cb);
				});
				// if after 
				setTimeout(() => {
					logger.error('Could not close connections in time, forcefully shutting down');
					setImmediate(cb);
				}, 1000);
			} else {
				setImmediate(cb);
			}
		}
	], (err) => {
		if (err)
			console.log(err);
		process.exit(code);
	})
};

// >>> TEST support
if (process.env.NODE_ENV === 'test') {

	// Report readiness state
	app.isReady = function () {
		return isReady;
	}

} else { // handle unhandled exceptions in normal mode here, otherwise let Mocha to handle them (beacuse Mocha assertions relies on this)
	
	process.on('uncaughtException', (err) => {
		const ts = new Date().toISOString();
		const msg = `**********\n${ts} Uncaught exception: ${err}\n${err.stack}\n`;
		console.error(msg);		// First here to be sure.
		if (logger && typeof logger === 'object') {
			try {
				logger.error(msg);
			}
			catch(err2)
			{
				console.error(`**********\n${ts} Exception while logging: ${err2}\n${err2.stack}\n`);
			}
		} else {
			const excFile = path.join(os.tmpdir(), 'Virtual_race_exception.log');
			fs.writeFileSync(excFile, msg, { flag: 'a' });
		}
		process.exit(1);
	});
}
// <<<< TEST support

process.on('unhandledRejection', (err, promise) => {
	const ts = new Date().toISOString();
	const msg = `**********\n${ts} Unhandled Rejection at: Promise: ${promise}\nreason: ${err}\n${err.stack}\n`;
	console.error(msg);		// First here to be sure.
	if (logger && typeof logger === 'object') {
		try {
			logger.error(msg);
		}
		catch(err2)
		{
			console.error(`**********\n${ts} Exception while logging: ${err2}\n${err2.stack}\n`);
		}
	} else {
		const excFile = path.join(os.tmpdir(), 'Virtual_race_rejection.log');
		fs.writeFileSync(excFile, msg, { flag: 'a' });
	}
	process.exit(1);
});

// Execute commands in clean exit (only synchronous activities are supported!!)
process.on('exit', () => {
	console.log('Bye');
});

// happens when you press Ctrl+C
process.on('SIGINT', () => {
	console.log('\nGracefully shutting down from SIGINT (Crtl-C)');
	app.gracefulExit();
});

// usually called with kill
process.on('SIGTERM', () => {
	console.log('Parent SIGTERM detected (kill)');
	app.gracefulExit(0);    // exit cleanly
});

process.on('warning', e => {
	console.warn(e.stack);
});



function initTemplate(template)
{
	const	templatePath = path.join(templateDir, template.name);
	const	fnm = path.join(template.name, 'track.json');
	try {
		const	buf = fs.readFileSync(path.join(templatePath, 'track.json')).toString();
		const	trackJSON = JSON.parse(buf);
		if (trackJSON.type !== 'Feature' || !trackJSON.geometry) {
			logger.warn(`Track ${template.name} .json is incorrect.`);
		} else if (trackJSON.geometry.type !== 'LineString' || !Array.isArray(trackJSON.geometry.coordinates) || trackJSON.geometry.coordinates.length < 2) {
			logger.warn(`Track ${template.name} .json has incorrect geometry.`);
		} else {
			let		ok = true;
			for (const coord of trackJSON.geometry.coordinates) {
				if (!Array.isArray(coord) || coord.length < 2) {
					logger.warn(`Track ${template.name} .json has incorrect coordinates.`);
					ok = false;
					break;
				}
			}
			if (ok) {
				template.config = {};
				try {
					template.config = JSON.parse(fs.readFileSync(path.join(templatePath, 'config.json')));
				}
				catch(err)
				{
					if (err.code !== 'ENOENT')
						logger.error(`${templatePath}/config.json: ${err.message}`);
				}

				template.track = trackJSON.geometry.coordinates;
				template.interpolator = new TrackInterpolator(template.track, template.config.overflow, template.config.flowback);
				template.gmTrack = template.interpolator.getGMTrack();
				template.vrActivities = new VRActivities(datastore, template.name);
				template.vrTargets = new VRTargets(datastore, template.name, template.interpolator.getLength());
				template.vrUsers = new VRUsers(datastore, template.name);
				template.vrResults = new VRResults(template.vrActivities, template.vrTargets, template.vrUsers);
				template.vrPaper = new VRPaper(templatePath, template.gmTrack, template.interpolator);

				app.use('/' + template.name, async(req, res) => clientRoute(template, req, res));
			}
		}
	}
	catch(err)
	{
		logger.warn(`Failed to load ${fnm}: ` + err.message);
	}
}

async function init() {
	// Custom error handler
	app.use((err, _req, res, _next) => {
		let msg = null;
		if (err) {
			if (typeof err === 'object') {
				if (process.env.NODE_ENV !== 'production' && err.stack && typeof err.stack === 'string') {
					msg = err.stack;
				} else if (typeof err.toString === 'function') {
					msg = err.toString();
				}
			} else if (typeof err === 'string') {
				msg = err;
			}
			logger.error(msg ? msg : JSON.stringify(err));
		}
		res.status(500).send(msg ? msg : 'Internal server error');
	});

	try {
		const	buf = fs.readFileSync(path.join(getPkgJsonDir(), 'secret/auth')).toString();
		let		sp = 0;
		for ( ; sp < buf.length; ) {
			let		ep = buf.indexOf('\n', sp);
			if (ep > 0)
				auths.push(buf.slice(sp, ep).trim());
			else
				auths.push(buf.slice(sp).trim());
			sp = ep + 1;
		}
//		logger.info(`${auths.length} auths read.`);
	}
	catch(err)
	{
//		logger.error('Error while getting auth: ', err);
	}

	templates = fs.readdirSync(templateDir, { withFileTypes: true })
		.filter(item => item.isDirectory() && item.name != 'admin' && item.name[0] != '.')
		.map(item => { return { name: item.name }});

	for (let template of templates) {
		const	templatePath = path.join(templateDir, template.name);
		const	fnm = path.join(templatePath, 'track.json');
		if (fs.existsSync(fnm)) {
			initTemplate(template);
		} else {
			logger.info(`Serving static files from /${template.name}`);
			app.use(`/${template.name}`, express.static(`./vrtemplates/${template.name}`));
		}
	}
	templates = templates.filter(item => item.track);
	logger.info(`Templates found: [${templates.map(item => item.name).join(', ')}]`);

	for (let template of templates) {
		logger.info(`Initialising ${template.name}`);
		await template.vrTargets.init();
		await template.vrUsers.init();
		await template.vrResults.init();
	}

	logger.info('Service is ready.');
	logger.info(`Running version ${pkg.version} of ${pkg.name}`);

	app.use((_req, res, _next) => {
		// Default error handler
		res.setHeader('Content-Type', 'text/html');
		res.status(404).send('<h1>Error: Not Found (404)</h1><p>The requested page could not be found.</p>');
	});
	
	logger.info('');
	isReady = 1;


	///////////////////////////////////////////////////////////////////////////////
	// Run HTTP server
	///////////////////////////////////////////////////////////////////////////////

	const port = process.env.PORT ?? 8080;
	logger.info(`Virtual race tries listening at port ${port}`);
	server = app.listen(port, (err) => {
		if (err) {
			logger.error(`Virtual race listening failed at port ${port}: `, err);
			app.gracefulExit(1);
		} else {
			logger.info(`Virtual race listening at port ${port}`);
			logger.info(`pid is ${process.pid}`);
			logger.info('');
		}
	});
	server.on("error", (err) => {
		logger.error('Virtual race failure: ', err);
	});
}


init();


module.exports = app;
