/*
	Copyright (c) 2022-2025 Sapikli (https://gitlab.com/sapikli)
	Released under European Union Public License 1.2
*/

const logger = require('./logger');
const { Datastore } = require('@google-cloud/datastore');
const { Kinds } = require('./kinds');


// Virtual race targets (milestones, sections)
class VRTargets {
	#datastore;
	#template;
	#trackLength;
	#targets;

	constructor(datastore, template, trackLength)
	{
		const	self = this;

		self.#datastore = datastore;
		self.#template = template;
		self.#trackLength = trackLength;
		self.#targets = [];
	}

	async init()
	{
		const	self = this;

		self.#targets = [];
		try {
			const query = self.#datastore.createQuery(self.#template, Kinds.Target);
			const [items] = await self.#datastore.runQuery(query);

			self.#targets = items;
			self.#targets.forEach(item => {
				item.distance = parseInt(item[Datastore.KEY].name);
			});

			if (!self.#targets.some(item => (item.finish)) && self.#trackLength && !Number.isNaN(self.#trackLength))
				logger.info(`Infinite ${self.#template}, track length: ${Math.round(self.#trackLength)}`);

			self.#targets = self.#targets
				.filter(i => Number.isInteger(i.distance) && i.name && i.name.length > 0)
				.sort((a, b) => (a.distance - b.distance));

		} catch(err) {
			logger.error('Error querying the Datastore: ', err);
		}
	}

	hasTargets()
	{
		const	self = this;

		return (self.#targets.length > 0);
	}

	getTargets()
	{
		const	self = this;

		return self.#targets;
	}
}


module.exports = VRTargets;
