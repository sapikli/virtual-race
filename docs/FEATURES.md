# Virtual Race Features & Configuration

## Registration

<p id="registration">By default, virtual races are open, e.g., anyone can participate. Whenever a new (nick)name is entered, that user gets registrated and some random marker colours set.</p>

To limit entries to registered users, set `membersonly: true` in the template's `config.json`. You may then add your participants manually, or you can provide a registration form for them. (A sample is found as `docs/sample-spartathlon/registration.html`.)

This way entrants can optionally enter their official names (for Participation Papers if you set up one), select their colours and icon (insignia), and their team (if you use it, like in Tug of War).

There are two options for their icons:

1. You have to create a `markers.js` in the template directory with the default marker definitions. It must contain SVG <symbol>s with `id="mapmarker-bg"`, `id="mapmarker"` and `id="mapmarker-fg"` for background, base marker and foreground, respectively. The base marker may be empty. Backgrounds and foregrounds are filled with the participant's corresponding colours.  
You may include an optional second marker image with `id="mapmarker3-bg"`, `id="mapmarker3"` and `id="mapmarker3-fg"`.

2. Icons are also generated from the initials of their names. The available options are managed automatically.

## Targets

<p id="targets">a.k.a. check points or milestones.</p>

By default, medals are given to the first 3 racers reaching each target.

These target medals can be customised by the `targetmedals[]` values in the template's `config.json` file. They can be either Unicode emojis or inline SVG images.

See the [admin interface](ADMIN.md#targets) documentation on setting up targets because you will need to use that for this feature.

In addition to its name, you may set an optional icon for each target. You also have the option to show/hide its appearance at various places of the page.

## Challenges

<p id="challenges">Activities over certain distances can be awarded.</p>

See the [admin interface](ADMIN.md#challenges) documentation on setting up challenges because you will need to use that for this feature.

In addition to the distance one must cover, you have to give a title and an optional icon.

## Monthly results

<p id="monthly">Medals are given to the three racers with the most distance in each month. The tooltip/popup displays this distance.</p>

In my experience, it's a very good motivation even for late comers who wouldn't have a chance to get target medals.

You can enable this feature by setting
```json
	"showmonthly": true
```
in the template's `config.json` file.

Monthly medals can be customised by the `monthlymedals[]` values at the same place. They can be either Unicode emojis or inline SVG images.

## Joint Effort

<p id="joint">A target distance participants have to reach together.</a>

This feature is meant to bring the community together by having a common goal. It may be running around/across your country, cycling around the Globe, or swimming across some ocean.

Distance covered and distance to go are displayed along with a progress bar, and the ETA is also calculated.

To enable this feature you have to add a numeric `jointdistance` member to `config.json` in the template directory. This value is the target and it's measured in the template's distance total units (kilometres or miles).

Appearance-wise two HTML elements are required: `id="jointprogress"` (visible by default) and a hidden `id="jointcompleted"` that is only shown when the target is reached.

Within the first one, there may be a `id="jointprogressbar"` whose style.width is set in percents and **aria-valuenow** is also set (in the range of 0 and 100). The text of `id="jointprogressbartext"` is changed to the percentage value.  
The text of `id="jointdone"` and `id="jointremaining"` elements are filled with the distance covered/remaining, respectively. The text of the `id="jointeta"` element is set to the estimated time of arrival. Any of these elements may be hidden if not wanted.

For the completed block, `id="jointduration"` is set to the number of days it took to finish the goal.

### Multiple waypoints

It is possible to have multiple waypoints, like going to and back. In this case a numeric array of `jointdistance` member is expected with the distances for each segment between consecutive waypoints.

All `id="jointprogress*"` elements must be multiplied for each segment by adding a `-` suffix with the 1-based index of the corresponding segment or waypoint. E.g., `id="jointprogress-1"`, `id="jointprogressbar-1"`, `id="jointprogressbartext-1"`, `id="jointdone-1"`, `id="jointremaining-1"`, `id="jointeta-1"` for the first part.

## Tug of War

<p id="tow">Activities are aggregated for two competing groups (team membership is configured for each user), like girls vs boys, sons vs fathers, etc.</p>

To enable this feature you will need to set the `team` property for users to 1 or 2 (other values don't participate) preferably at registration, and to have an `id="tow"` element.

Within this, `id="towbar1` belongs to the first group and its **width** attribute is set in percentages. Similarly, `id="towbar2"`'s **x** attribute and the calculated **width** is adjusted. (The tie is at 50%.)

The text of `towtext1` and `towtext2` is set to the cumulative distances.

## Blackout

<p id="blackout">Participants see only their own results during a blackout.</p>

There is a feature to hide the result list and the map for a blackout in order to make the finish more exciting. During this period, only the longest distances and the exercises of the current user are shown. It is also useful for preventing players from deceiving their opponents and entering several exercises only last minute.

The `blackoutfrom` value of the template's `config.json` is used to turn on this behaviour. The optional `blackoutuntil` value is the time when full results are shown again. Both are expected to be in YYYY-MM-DD order.

During a blackout, the (by default hidden) `id="blackout"` element is displayed. You can use this to inform the participants.

The code calls an optional `startCountdown()` function with the end Date of the race. You may simply display the remaining time, or implement a countdown animation.

## Participation Paper

<p id="paper">Finishers may download a Participation Paper (or Certificate of Merit, or Diploma) displaying their result details and showing the track with their steps.</p>

To make it accessible to the finishers, put a button with `"onClick="return printPaper();"` on the `id="racecompleted"` alert.

You have to put a `paper.svg` in the template directory containing placeholder tags that are replaced with the corresponding data and a `paper.json` to configure the appearance. Read more on them in [Templates and Configuration](TEMPLATE.md).
