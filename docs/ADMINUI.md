# Virtual Race Admin UI

Though there is a web ["API"](ADMIN.md) for Virtual Race for all necessary maintenance functions,
there is also a UI to make its use easier.

On loading the main page, authentication is required. As at all other places, a string is to be entered here
that is contained in the `secret/auth` file.  
For your local development runs/tests you may append it as an URL parameter (`file://adminui.html?auth=verysecret`) and bookmark it as such. However, **don't use this method on live servers** because Google will log your credentials as part of the request URL.

## Template functions

You can create a **Full Export**: all contents of the Datastore will be dumped to a JSON file.

The **Export Activities** button saves all activities into a CSV text file. It can be used to make global checks, or create statistics/graphs.  
Now distances are exported in metres as the Admin UI doesn't handle different units for different tamples.

The **Add Activities** button enables you to import activities from a CSV text file. The separator can be semicolon (;) or a tabulator character. String fields may be enclosed in quotes but it's not necessary. The order of the columns is the same as for export:
* Name
* Date (YYYY-MM-DD)
* Distance (in metres)
* Submit timestamp is optional (which may be either in Unix timestamp milliseconds, or as an ISO date-time string). In case of a missing value, the current timestamp will be used. If you provide them, however, the values must be unique.

Beware, the rows to be imported are not checked against existing activities here so you may end up with duplicates.

UTF-8 encoding is expected.

<hr>

By default, the **User List** page is displayed. You may want to change the **Template** in the top-left corner. To return to this page just re-select the template from the drop-down list.

![Users](adminui_users.png)

<hr>

For each user, you have a button to Edit their properties.

![Users](adminui_edituser.png)

The `name`` cannot be modified as it is the key.

`Official name` is used only on the Participation Paper and it's also optional in order to ease GDPR stuff.

`Marker colours` may be changed by clicking on the buttons.

`Team` is a numeric field. Right now it is only used for Tug of War: 1 and 2 are the two sides, 0 is for non-participants.

`Marker` may be selected for the user. The circle in the first row belongs to "mapmarker" of the template, while the square means "mapmarker3" (if present) in `markers.js` of the given template.  
It's not implemented yet to show the actual icons, as they are different for each template.

<hr>

By pressing the Activities button, you can see a **list of activities** for the corresponding user:

![Users](adminui_activities.png)

You also have the option to Edit the `Date` or `Distance` of them:

![Users](adminui_editactivity.png)
