# Virtual Race Step-by-Step Set-up Guide

As a first step, you need to fork this repository on GitLab or GitHub so that you can have your own static web page. Set up Pages and enable automatic CI/CD (otherwise you'll need to do it manually).

You also have to create a Google Cloud Platform account. You'll get a few hundred dollars of credits but you won't use that.

In your repo project settings, create a variable `VR_APPENGINE_BASEURL` that points to your app engine (something like "https://&lt;funny name&gt;-&lt;project ID&gt;.ew.r.appspot.com/"). This variable shouldn't be protected because the default version tagging that triggers the build does not create a protected tag.

Modify `package.json` to reflect your own repository. Run `npm install` to get node_modules locally.

Put your template(s) in the `vrtemplates` directory. It's recommended to use a separate (private) repository for this purpose and attach it as a submodule. This way GitLab deployment will automatically pull/refresh templates. See the [template documentation](TEMPLATE.md) on how to create one.  
There is a sample default `baseurl.js` file in the `public` directory that you can use for your local tests.

More on enabling or configuring different features see [Features &amp; Configuration](FEATURES.md).

Put an `auth` file in the `secret` directory. This file should contain credentials (one each line) to be accepted by the backend admin HTTP interface. As the communication is through HTTPS, no encoding is used and the admin request POST parameter is simply compared to this string. I use a Base64-encoded string of a username:password pair.

Put your static web service base address in the `baseurl` file in the `secret` directory, like "https://mydomain.gitlab.io". This value is used to create a CORS policy for the static content.

Once your backend is up, you can use the [admin interface](ADMIN.md) to add checkpoints/milestones, modify users and do other administration stuff.

There is a sample batch file `deploy.bat` with the usual steps for deploying the server. Change your repo URL and Google Cloud bucket name.
The first command creates a new minor version.
The second command tags it on GitLab which triggers the deployment of the static GitLab Pages (with the templates).
Finally, the third command deploys the AppEngine from your local files.
