@REM Increment version number
npm version patch -m "Upgrade to %%s" | more

@REM Push it to the repo and tag this version
git.exe push --tags --verbose git@gitlab.com:myname/myproject.git main:main || GOTO :EOF

@REM Deploy the AppEngine
gcloud app deploy -q --bucket=gs://mybucket-123456
