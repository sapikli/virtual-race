# Virtual Race Templates and Configuration

A virtual race is configured through a so-called template. Several races (templates) can run in parallel, independent from each other.

By default, these are stored in the `vrtemplates` folder. It is referred by `src/server.js` for the backend and in `.gitlab-ci.yml` for the deployment. I recommend putting templates to a separate repository and create a git submodule reference to this folder. This way the deployment process will automatically pull templates and include them.

Each template resides within `vrtemplate` as a subfolder. The name of each directory is used to form the base URL for a race. You may put any additional content files (logo images, etc.) here. The following files are needed for a race:

## config.json

It is a [JSON](https://www.json.org/json-en.html) object file containing basic server-side configurations for the template.

The following properties are supported:

`membersonly`: This boolean value determines if only existing (or registered) participants/users/members can add activities. The default value is `false` (public access).

`overflow`: This boolean value controls the display behaviour of participants completing the race. With the default `false` value all finishers are fixed in the Finish. Setting this value to `true` enables them to continue moving along the track.

`flowback`: This boolean value controls overflow behaviour. By default, round track is assumed and the participant starts another lap. If this value is `true`, the participant turns at the Finish and moves backward.

The duration of the race can be specified by the `startdate` and `enddate` values. These are expected to be in YYYY-MM-DD order. The end day is inclusive.

There is a feature to hide the result list and the map for a blackout in order to make the finish more exciting. During this period, only the longest distances and the exercises of the current user are shown. It is also useful for preventing players from deceiving their opponents and entering several exercises only last minute. The `blackoutfrom` value is used to turn on this behaviour. The optional `blackoutuntil` value is the time when full results are shown again. Both are expected to be in YYYY-MM-DD order.

The distance for the Joint Effort can be set by the `jointdistance` value. Its units are the same that is displayed for sum distances.

The default milestone icon can be set with the `milestoneicon` property. It can be a Unicode emoji, inline SVG, etc.

The handicap column for the leader can be customised via the `leadersign` setting. It may be simply blank (&amp;nbsp;), a zero, a dash, or a character corresponding to the race type, like '&amp;#x1F3C3;' = &#x1F3C3; for a running event. Inline SVG images also may be used.

Similarly, for everyone who's completed the race, the `completedsign` is displayed here. It may be left blank, or use the text "completed", or some Unicode emojis, etc.

Just like for `leadersign`, there is a `hardworksign` setting to honour the participant with the most activities. The tooltip text can be set by `index.html/HardWorkText`.

Target medals can be customised by the `targetmedals[]` values. They can be either emojis or inline SVG images.

`showmonthly`: This boolean value enables monthly medals. They are awarded for each month by the total distance covered in that month. Usually either distance-based targets or monthly medals are used but they are not mutually exclusive.

Monthly medals can be customised by the `monthymedals[]` values. They can be either emojis or inline SVG images.

Icons for top finishers can be customised using the `trophies[]` values. They can be either emojis or inline SVG images.  
Other finishers get the `finishermedal` displayed.

The finish map marker may be hidden (for surprise distances or for "unlimited" races) by setting `hidefinish` to `true`.

## favicon.ico

It's a usual site icon, typically 16x16 pixels in size. If you don't want/have one, just remove the reference from `index.html`.

## track.gpx

I usually provide the track of the whole race as a downloadable static file, typically in [GPX](https://www.topografix.com/gpx.asp) file format, understood by most sports apps and smartwatches. If you don't want/have one, just remove the reference from `index.html`.

## track.json

This file is compulsory because it defines the race track. Its format is [GeoJSON (RFC 7946)](https://tools.ietf.org/html/rfc7946), the coordinates are expected to be in [WGS-84](https://epsg.io/4326) reference system, in longitude-latitude (x-y) order.

A single LineString feature is expected. It may be either open or closed (for a roundtrip).

One may provide a `properties` object to define the display style of the track on the map (otherwise defaults will be used in the script). It's an example of a blue dashed line:

```json
"properties": {
    "color":     "blue",
    "dashArray": "4",
    "weight":    2
}
```

## index.html

And this is where the entire look is born.

As a starter, define your own HTML &lt;meta&gt; "description" and "keywords", along with the page &lt;title&gt;. In the &lt;body&gt;, replace/translate texts as you wish, change/add images. In short, do a basic HTML editing as you wish.

The &lt;head&gt; section contains a short &lt;script&gt; section for configuration, customisation and localisation.

* `rootURL` is the entry point of the backend. It is usually the template name.

* `lsName` is a key for the client browser local storage where the entered name is stored (or not). It's recommended to use a different one for each race so one person may use different names for different races.

**Map markers** may be also customised here.

* The size of the map position markers can be defined by `MarkerSize`, in pixels. Position of the current user may be highlighted by a bigger marker, defined by `OwnMarkerSize`.

* The anchor (origin) of the marker image is set by `MarkerAnchorX` and `MarkerAnchorY`. These values correspond to the regular `MarkerSize`.

**Units** can be customised in two ways. One is `DistanceMultiplier` that specifies how many metres the input/display units are. Moreover, the total distances may be in different units, with an additional `SumDivisor`.

It's important to set up `DistanceMultiplier` properly because virtual track progression is calculated in metres internally.

Some examples:

```javascript
    // Kilometres everywhere, like running, cycling, etc.
    const DistanceMultiplier = 1000;
    const SumDivisor = 1;
```

```javascript
    // The same with miles
    const DistanceMultiplier = 1609.344;
```

```javascript
    // Use metres but kilometres for totals, like for swimming
    const DistanceMultiplier = 1;
    const SumDivisor = 1000;
```

```javascript
    // Use yards and nautical miles, like for imperial swimming
    const DistanceMultiplier = 0.9144;
    const SumDivisor = 2025.3718285214;
```

* A `MaxDistance` setting is used to limit user inputs. This value is in client UI units (e.g., kilometres or miles).

* Along with the distance units, you may want to configure the map scale bar. Set `ShowMetricBar` to true in order to display metric units, and set `ShowImperialBar` to true in order to display imperial units. Both may be set at the same time, or none to hide to scale bar.

* The localisation part should be straightforward, you only need to translate these texts. The script detects if it's a roundtrip and uses `StartFinish` on a single marker instead of the usual `Start` and `Finish`.

Tip: You may want to provide multiple HTML files, like in different languages, or one using metric and the other using imperial metrics, or one in the default light theme and another one in dark theme.

## index.html / Features

You can omit/turn off certain blocks (lists or even the map) simply by removing their entire heading and &lt;div&gt; from the HTML source.

The order or position of each block is also flexible.


## manifest.json

This file is for a Progressive Web App (PWA) manifest. If you are unfamiliar with this concept, the attributes in this file are explained [here](https://web.dev/articles/add-manifest).


## markers.js

The map marker icon is defined within the `markers.js` file. It is to share the same resource between various parts of the system. The format is a string constant containing multiple &lt;symbol&gt; definitions.

The icon in [SVG](https://www.w3.org/Graphics/SVG/) format is to be displayed on the map in colours of the participants. In order to make it versatile, it is combined from three parts in the following draw order:

The first part with `id="mapmarker-bg"` will have its (default) fill colour set to the participant's background colour. All other colours (like stroke) are left unchanged.

The second part is `id="mapmarker"` is drawn as it is, without any colour changes.

The third part with `id="mapmarker-fg"` will have its (default) fill colour set to the participant's foreground colour. All other colours (like stroke) are left unchanged.

All three parts are optional. The corresponding &lt;symbol&gt; tag should be present but it may be empty.


## registration.html

Do the same design customisation for the registration page. By default, the race is open, e.g., no registration is required so the reference to this page may be removed from `index.html`. See also `membersonly` in `config.json`.

However, it may be useful to have one: give the to-be-participants a short overview, show some FAQ, and even let them to choose their colours and insignia/jersey/marker,
along with their preference to participate in the Tug of War. All parts/features are optional (except the participants's name).

The built-in markers and a few settings are duplicated between this file and `index.html` for technical reasons. Please keep the synchronised.


## paper.json

This file contains customisations and localisations to the server-side-generated Participation Paper. It is a [JSON](https://www.json.org/json-en.html) object file.
The available properties are:

`width`, `height`: They are the width and height of the result [SVG](https://en.wikipedia.org/wiki/SVG) file. Typically matches the `viewBox` width/height in the SVG template file.

`mapscale`: This value is the ratio of the SVG dimensions and the race track to be drawn. If this value is omitted, no track is drawn.

`mapcenterx`, `mapcentery`: These values specify the center point of the track map. The defaults are half of `width` and `height`, respectively.

`trackpoint`: It is an SVG element to be drawn at the participant's steps. The placeholders `{{x}}` and `{{y}}` are replaced by the actual coordinates.

`places`: It is an array of strings with at least one element. It may contain Unicode emojis, etc. It is displayed for the first N participants only. Similar to the `Trophies` array in `index.html`.

`longest`: It is an array of objects with `text` and optional `icon` property to be awarded for the longest activities. The array may be empty. This award is displayed for the first N participants only.  
The strings may contain `{{km}}` and `{{m}}` placeholders that are replaced by the kilometre (rounded down) and metre (zero-padded to 3 digits) values of the longest activity.

`medals`: It is an array of exactly 3 strings with the medals displayed at target results and monthly challenges. Similar to `TargetMedals` and `MonthlyMedals` in `index.html`.

`hardwork`: It is an object with `text` and optional `icon` property displayed for the user with the most activities. Similar to `HardworkText` and `HardworkSign` in `index.html`. This object may be omitted if no such award is to be given.

`badges`: This object configures the display of the various medals and awards (everything except the finish result). Its members are:

The `row` string contains the starting &lt;tspan&gt; of these medals. In practice, the `x` attribute is fixed and the `dy` attribute is used for line feed.

It is possible to break long lines in two. The optional `firstrow` and `secondrow` strings can be used for this case. The spacing between the two lines may be different from the default line feed and indentification may be used.

The optional `upwards` boolean property is to tell if the badges are aligned to the bottom, e.g., drawn from the bottom upwards. In this case, badges with text broken into two lines (see above) are displayed in the correct order.

It is possible to have a monthly bar chart. The `monthly` object is used to configure its appearance. Its members are:

`xstart`, `ystart`: Top-left coordinates of the chart.

`xoffs`: Horizontal displacement of the bars.

`ybars`: Relative bottom coordinate of the bars.

`barwidth`, `barheight`: Dimensions of a full-height bar.

`barstyle`: A CSS style for drawing the bars.

`monthtransform`: If specified, month numbers will be added. It is a CSS transform (or multiple transforms).

`romanmonths`: If set to `true`, Roman numbers will be used as ordinals.

`yoffsmedals`: Relative offset for monthly medals above/over bars.

`ydistance`: Relative offset for the monthly distance texts.


## paper.svg

You can put the entire Participation Paper graphics in an SVG file, including background image, text styles, filters, etc.

There are placeholders that are replaced by the generated values.

`{{name}}`: It will be replaced to the name of the participant. Or to the optional official name.

`{{userfg}}`, `{{userbg}}`: Foreground and background colours of the participant. Can be used draw a jersey/flag in his/her colours.

`{{place}}`: The finish place of the participant. See `places` above.

`{{days}}`: The number of days it took the participant to finish the race.

`{{count}}`: The number of activities it took the participant to finish the race.

`{{totalcount}}`: The total number of activities, including the ones entered after (first) finishing the race. It may be used for non-finish (infinite) races, too.

`{{totalkm}}`: The kilometre part of the total distance.

`{{totalm}}`: The metre part of the total distance, 3 digits.

`{{totalmtrim}}`: The metre part of the total distance, trailing zeros removed.

`{{badges}}`: A series of &lt;tspan&gt; entries with the medals/badges/awards of the participant. See `badges.row`, etc., above.

`{{monthly}}`: A monthly bar chart.

The map part also has placeholders:

`{{track}}`: It is the data (`d=`) part of a &lt;path&gt;.

`{{points}}`: A series of elements for the participant's steps. See `trackpoint` above.

`{{startx}}`, `{{starty}}`, `{{finishx}}`, `{{finishy}}`: The start and finish coordinates of the race.

