# Virtual Race Admin Functions

There is an admin HTTP interface to the backend. You can send these commands with tools that can handle POST requests, like [Postman](https://www.postman.com/products/). The path of this interface is `/admin` on the backend server.

Commands are expected to be in `application/json` Content-Type.

The JSON object must contain an `auth` string that is compared against the contents of an auth file on the backend server.

The `command` property is also compulsory.

The `template` property selects which virtual race is to be administered.

Example:
```json
{
    "auth":     "dXNlcjpwYXNzd29yZA==",
    "template": "sample-spartathlon",
    "command":  "list",
    "kind":     "activity"
}
```

---

Available commands:

## listtemplates

This command returns a JSON list of all templates available on the server.
Naturally, the `template` property is not used here.

## clearall

This command clears all activities from this race/template. Use with care.

## addactivities

This command can be used to add one or more activities. The `data` property may contain an activity object or an array of such objects. Each object has the following properties:

* `user`: Name of the user.

* `date`: Date of the activity, in YYYY-MM-DD format.

* `distance`: Distance of the activity, in integer metres.

* `submit`: Submission time of the activity, in milliseconds Unix timestamp. Be aware that this value is used as the primary key for activities so all objects must have different values.

Users not yet existing will be automatically added.

## list

This command simply lists all entities in the Datastore of the specified `kind` related to this template. Current item kinds: `"activity"`, `"challenge"`, `"target"`, `"user"`.

Additional filter parameters may be specified:

* `datefrom`, `dateto`: Match entries that have a `date` property and that are at or later than `datefrom` (if supplied) and at or before `dateto` (if supplied). As user-input activity dates are stored in YYYY-MM-DD format, these properties are also expected as YYYY-MM-DD.

* `submitfrom`, `submitto`. Similarly, match entries that have a `submit` property and are between these values. Submit times are in millisecond Unix timestamps (the default ECMAScript Date internal).

* `user`. If the entry has a `user` property, it has to match the supplied value. Case sensitive.

## delete

There are two modes this command may be used to delete entries.

One possibility is to address a single entry.

* It can be done by a full `key` object what has a `namespace` string (matching the template name) and a `path` two-item string array member. The first string is expected to be the item kind, the second string is the item name. Example: *{ "key": { "namespace": "sample-spartathlon", "path": [ "user", "Forrest Gump" ] }*

* The second version expects separate `kind` and `key` string members for simplicity. Example: *{ "kind": "user", "key": "Forrest Gump" }*

* There is a legacy version of a single `key` string member by appending the kind and item name with an underscore character. Example: *{ "key": "user_Forrest Gump" }*

* The fourth version uses a two-item string array for the `path` member as mentioned at the first option. Example: *{ "path": [ "user", "Forrest Gump" ] }*

The other option is to use filtering with at least one of the parameters mentioned at the **list** command in addition to `kind`.

## set

This command is to add/overwrite ("upsert") any kind of objects.

* `key`, `kind` or `path`: This property is compulsory to identify the entity. In case of new entities you must adhere to some conventions. See below.

* `data`: This is the new attribute object. See its description below.

## delprop

This command can be used to delete a property from any kind of objects. Based on the filter, properties of multiple objects may be deleted with a single command.

* `key`, `kind` or `path`: It must be specified to identify the activity to be modified. See **delete** for possible values.

* `property`: Name of the property to be deleted.

## setprop

This command can be used to set/modify a property in any kind of objects. Based on the filter, properties of multiple objects may be set/modified with a single command.

* `key`, `kind` or `path`: It must be specified to identify the activity to be modified. See **delete** for possible values.

* `property`: Name of the property to be set/modified.

* `data`: The new value/data.

## administer

This command may be used after deleting or importing a great deal of data. It recalculates achievements and re-distributes badges and medals.

It is automatically called after after deleting or modifying some activities or targets.

## export

This command dumps all objects in the template's Datastore as a JSON object.

The results may be used as a backup (like calling `export` daily by curl), to copy targets, users and/or activities to another race, or for mass editing.

## import

This command can be used to mass import Datastore entities.

* `clear`: This optional property can be used to empty the template's Datastore before adding new items.

* `data`: This object should contain the keys and values to be imported. Any existing keys are overwritten with the new value.

See `install.json` in the sample template directory for an example.

## getresults

This command returns the result JSON object just like the `/get` call used by the clients. The only difference is that the admin interface returns results even during the blackout.

---

# Description of the Datastore objects:

## Activities (runs, rides, swims, etc.)

The kind is `"activity"`, the item key/name is an auto-generated unique value. Its only purpose is to avoid collisions when multiple activities are being submitted at the very same moment. It is basically the current timestamp, a simple hash of the user name, and a hash generated from the App Engine instance ID.

The value members are documented at the **modify** command.


## Challenges (distance for a single activity)

<p id="challenges">The kind is `"challenge"`, the item key/name is *distance in metres* (as string).</p>

These items must have an `icon` and a `name` property as they will be displayed on the web page.

The participant is credited for the challenge with the greatest distance only.


## Targets (checkpoints, milestones)

<p id="targets">The kind is `"target"`, the item key/name is *distance in metres* (as string).</p>

There are two types of targets.

One is the finish. It is automatically added if not present yet. It is distinguished by the
```json
"finish": true
```
property.

Other targets should have a user-friendly `name` property as it will be displayed on the web page.

An optional `icon` property may be used for the given target instead of the default flag icon.

A target may have a `showonmap` boolean property to display a marker on the map for this target (the default is `false`). If the target has an `icon`, it will be used instead of the default red square. Use the `iconsize` attribute to specify the map marker size (in pixels).

Further optional switches (false by default):

`nomedal`: The target will not have medals awarded to the first achievers. It's only the completion that counts.

`hidealert`: Use this property to disable displaying a completion alert to the participant.

`hideonlist`: This attribute hides the target completion on the leaderboard (result list).

`hideonmine`: This attribute hides the target completion on the "Mine" list. 


## Users (participants, competitors)

The key kind is `"user"`, the item key/name is the participant's name. It may contain spaces, punctuation, accented or other non-ASCII characters except storage-related characters like  / { } _ (slash, curly breaces, underscore).

All properties are optional.

Example:

```json
{
    "bgcolor":"#a500f2",
    "fgcolor":"#ffffff",
    "official":"James Bond",
    "team":1
}
```

`bgcolor` and `fgcolor` are used to mark the user. By default a colour pair is generated from the user name. It may be changed to reflect the colours of the participant's jersey, or simply for easthetic reasons.

`official`: Even though the name of the participant can be a nickname or a short one (like given name only), it may be wished to display the official/full name on the Participation Paper.

`svg` is an optional property. It may contain a per-user map marker SVG image. The same rules apply as mentioned in the [template documentation](TEMPLATE.md). As it is used as an inline &gt;svg&lt; element, omit the &lt;?xml&gt; and &lt;!DOCTYPE&gt; elements.  
A suggested way for common SVG markers is to put &lt;symbol id="whatever"&gt; definitions in the [index.html](TEMPLATE.md) file and only &lt;use href="#whatever"/&gt; them within the &lt;svg&gt;.  
To share and reuse SVG parts (&lt;symbol&gt;s) amongst users, the supplied inline SVG may contain "{{bgcolor}}" and "{{fgcolor}}" placeholders that are replaced to the user's respective colour values.

`team` is an optional property for team-based events, like Tug of War calculations. Here members of the first (left side) group have a value of 1, members of the second (right side) group have a value of 2. Otherwide the user does not participate.
